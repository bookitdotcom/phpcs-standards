<?php

/**
 * Base functional test class for sniffs
 */
abstract class SniffTest extends PHPUnit_Framework_TestCase {

	/**
	 * Gets the name of the Sniff class
	 *
	 * @return string
	 */
	public static function getSniffName() {
		throw new \RuntimeException('Sniff name not defined');
	}

	/**
	 * Asserts that a given file does not produce any errors
	 * or warnings for the current sniff
	 *
	 * @param string $file The file to sniff
	 * @throws PHPUnit_Framework_AssertionFailedError
	 * @throws RuntimeException If the JSON report cannot be parsed
	 */
	public function assertSniffPasses($file) {
		$report = self::runSniff($file);
		parent::assertEquals(0, $report->files->{$file}->errors);
		parent::assertEquals(0, $report->files->{$file}->warnings);
	}

	/**
	 * Asserts that a given file produces an expected error
	 *
	 * @param string $file The file to sniff
	 * @param int $line The line number where we expect a violation
	 * @param string $code The violation code that we expect
	 * @param string $message The message we expect
	 * @param int $severity The violation severity we expect
	 * @throws PHPUnit_Framework_AssertionFailedError
	 * @throws RuntimeException If the JSON report cannot be parsed
	 */
	public function assertSniffError($file, $line, $code = '', $message = '', $severity = NULL) {
		self::assertSniffResult($file, $line, 'ERROR', $code, $message, $severity);
	}

	/**
	 * Asserts that a given file produces an expected warning
	 *
	 * @param string $file The file to sniff
	 * @param int $line The line number where we expect a violation
	 * @param string $code The violation code that we expect
	 * @param string $message The message we expect
	 * @param int $severity The violation severity we expect
	 * @throws PHPUnit_Framework_AssertionFailedError
	 * @throws RuntimeException If the JSON report cannot be parsed
	 */
	public function assertSniffWarning($file, $line, $code = '', $message = '', $severity = NULL) {
		self::assertSniffResult($file, $line, 'WARNING', $code, $message, $severity);
	}

	/**
	 * Asserts that a given file produces an expected error or warning
	 *
	 * @param string $file The file to sniff
	 * @param int $line The line number where we expect a violation
	 * @param string $level The level of the expected violation, either ERROR or WARNING
	 * @param string $code The violation code that we expect
	 * @param string $message The message we expect
	 * @param int $severity The violation severity we expect
	 * @throws PHPUnit_Framework_AssertionFailedError
	 * @throws RuntimeException If the JSON report cannot be parsed
	 */
	private function assertSniffResult($file, $line, $level, $code = '', $message = '', $severity = NULL) {
		$report = self::runSniff($file);
		$or = array();

		foreach ($report->files as $reportFile => $fileData) {
			foreach ($fileData->messages as $reportEntry) {
				$constraints = array();

				$constraints[] = new PHPUnit_Framework_Constraint_Attribute(
					new PHPUnit_Framework_Constraint_IsEqual($level),
					'type'
				);

				$constraints[] = new PHPUnit_Framework_Constraint_Attribute(
					new PHPUnit_Framework_Constraint_IsEqual($line),
					'line'
				);

				if ($code !== '') {
					$constraints[] = new PHPUnit_Framework_Constraint_Attribute(
						new PHPUnit_Framework_Constraint_IsEqual(static::getSniffName() . '.' . $code),
						'source'
					);
				}

				if ($message !== '') {
					$constraints[] = new PHPUnit_Framework_Constraint_Attribute(
						new PHPUnit_Framework_Constraint_StringContains($message),
						'message'
					);
				}

				if ($severity !== NULL) {
					$constraints[] = new PHPUnit_Framework_Constraint_Attribute(
						new PHPUnit_Framework_Constraint_IsEqual($severity),
						'severity'
					);
				}

				$and = new PHPUnit_Framework_Constraint_And();
				$and->setConstraints($constraints);

				$or[] = $and->evaluate($reportEntry, '', TRUE);
			}
		}

		parent::assertContains(
			TRUE,
			$or,
			sprintf('Failed asserting that Sniff %s raises %s with code %s in file %s on line %d', static::getSniffName(), $level, $code, $file, $line)
		);
	}

	/**
	 * Runs PHP_CodeSniffer for a given sniff and file
	 *
	 * This uses the PHP_CodeSniffer command line and parses a JSON report
	 * because PHP_CodeSniffer does not have a very friendly API for
	 * extracting violation data
	 *
	 * @param string $file The file to sniff
	 * @return stdClass
	 * @throws RuntimeException If the JSON report cannot be parsed
	 */
	private function runSniff($file) {
		$command = array('vendor/bin/phpcs', '--standard=BookIt/', '--report=json');
		$command[] = escapeshellarg('--sniffs=' . static::getSniffName());
		$command[] = escapeshellarg($file);

		$report = shell_exec(implode(' ', $command));
		$report = str_replace("\n", "\\n", $report);
		$report = json_decode($report);

		if ($report === NULL) {
			throw new \RuntimeException(
				'Could not decode JSON report',
				json_last_error()
			);
		}

		return $report;
	}
}

