<?php

namespace Foo {
	const FooConst = 'Hello';

	class Bar {
		const BarConst = 'World!';
	}
}
