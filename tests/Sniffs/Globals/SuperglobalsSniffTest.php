<?php

class SuperglobalsSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Globals.Superglobals';
	}

	public function testGetInGlobalScope() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/Superglobals.FoundInGlobalScope.php',
			3,
			'FoundInGlobalScope',
			'GetRegistry::get()'
		);
	}

	public function testPostInGlobalScope() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/Superglobals.FoundInGlobalScope.php',
			4,
			'FoundInGlobalScope',
			'PostRegistry::get()'
		);
	}

	public function testCookieInGlobalScope() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/Superglobals.FoundInGlobalScope.php',
			5,
			'FoundInGlobalScope',
			'CookieRegistry::get()'
		);
	}

	public function testServerInGlobalScope() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/Superglobals.FoundInGlobalScope.php',
			6,
			'FoundInGlobalScope',
			'ServerRegistry::get()'
		);
	}

	public function testRequestInGlobalScope() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/Superglobals.FoundInGlobalScope.php',
			7,
			'FoundInGlobalScope',
			'appropriate registry object'
		);
	}

	public function testEnvInGlobalScope() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/Superglobals.FoundInGlobalScope.php',
			8,
			'FoundInGlobalScope',
			'see an Architect'
		);
	}

	public function testSessionInGlobalScope() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/Superglobals.FoundInGlobalScope.php',
			9,
			'FoundInGlobalScope',
			'SessionRegistry::get()'
		);
	}

	public function testGetInFunction() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/Superglobals.FoundInRestrictedScope.php',
			4,
			'FoundInRestrictedScope',
			'GetRegistry::get()'
		);
	}

	public function testPostInFunction() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/Superglobals.FoundInRestrictedScope.php',
			5,
			'FoundInRestrictedScope',
			'PostRegistry::get()'
		);
	}

	public function testCookieInFunction() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/Superglobals.FoundInRestrictedScope.php',
			6,
			'FoundInRestrictedScope',
			'CookieRegistry::get()'
		);
	}

	public function testServerInFunction() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/Superglobals.FoundInRestrictedScope.php',
			7,
			'FoundInRestrictedScope',
			'ServerRegistry::get()'
		);
	}

	public function testRequestInFunction() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/Superglobals.FoundInRestrictedScope.php',
			8,
			'FoundInRestrictedScope',
			'appropriate registry object'
		);
	}

	public function testEnvInFunction() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/Superglobals.FoundInRestrictedScope.php',
			9,
			'FoundInRestrictedScope',
			'see an Architect'
		);
	}

	public function testSessionInFunction() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/Superglobals.FoundInRestrictedScope.php',
			10,
			'FoundInRestrictedScope',
			'SessionRegistry::get()'
		);
	}

	public function testGetInClassMethod() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/Superglobals.FoundInRestrictedScope.php',
			15,
			'FoundInRestrictedScope',
			'GetRegistry::get()'
		);
	}

	public function testPostInClassMethod() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/Superglobals.FoundInRestrictedScope.php',
			16,
			'FoundInRestrictedScope',
			'PostRegistry::get()'
		);
	}

	public function testCookieInClassMethod() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/Superglobals.FoundInRestrictedScope.php',
			17,
			'FoundInRestrictedScope',
			'CookieRegistry::get()'
		);
	}

	public function testServerInClassMethod() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/Superglobals.FoundInRestrictedScope.php',
			18,
			'FoundInRestrictedScope',
			'ServerRegistry::get()'
		);
	}

	public function testRequestInClassMethod() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/Superglobals.FoundInRestrictedScope.php',
			19,
			'FoundInRestrictedScope',
			'appropriate registry object'
		);
	}

	public function testEnvInClassMethod() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/Superglobals.FoundInRestrictedScope.php',
			20,
			'FoundInRestrictedScope',
			'see an Architect'
		);
	}

	public function testSessionInClassMethod() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/Superglobals.FoundInRestrictedScope.php',
			21,
			'FoundInRestrictedScope',
			'SessionRegistry::get()'
		);
	}
}
