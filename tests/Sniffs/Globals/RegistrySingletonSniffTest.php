<?php

class RegistrySingletonSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Globals.RegistrySingleton';
	}

	public function testPostRegistryInGlobalScope() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/RegistrySingleton.FoundInGlobalScope.php',
			8,
			'FoundInGlobalScope'
		);
	}

	public function testPostRegistryInClassMethod() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/RegistrySingleton.FoundInRestrictedScope.php',
			10,
			'FoundInRestrictedScope'
		);
	}

	public function testPostRegistryInFunction() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/RegistrySingleton.FoundInRestrictedScope.php',
			15,
			'FoundInRestrictedScope'
		);
	}

}
