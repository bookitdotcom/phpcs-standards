<?php

class GlobalConstantSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Globals.GlobalConstant';
	}

	public function testNamespacedConstants() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/GlobalConstant.pass.php'
		);
	}

	public function testGlobalConstantWithConstKeyword() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/GlobalConstant.Found.php',
			5,
			'Found'
		);
	}

	public function testGlobalConstantWithDefine() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/GlobalConstant.Found.php',
			7,
			'Found'
		);
	}

}
