<?php

class ClassOmitClosingTagSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.PHP.OmitClosingTag';
	}

	public function testCloseTag() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OmitClosingTagSniff.CloseTag.php',
			8,
			'CloseTag'
		);
	}

	public function testNoCloseTag() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/OmitClosingTagSniff.pass.php'
		);
	}
}

