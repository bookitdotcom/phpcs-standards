<?php

class UpperCaseConstantSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.PHP.UpperCaseConstant';
	}

	public function testLowercaseTrue() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/UpperCaseConstantSniff.Found.php',
			3,
			'Found',
			'expected "TRUE" but found "true"'
		);
	}

	public function testLowercaseFalse() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/UpperCaseConstantSniff.Found.php',
			4,
			'Found',
			'expected "FALSE" but found "false"'
		);
	}

	public function testLowercaseNull() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/UpperCaseConstantSniff.Found.php',
			5,
			'Found',
			'expected "NULL" but found "null"'
		);
	}

	public function testClassInterfaceAndNamespaceDeclarations() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/UpperCaseConstantSniff.pass.declarations.php'
		);
	}

	public function testClassProperties() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/UpperCaseConstantSniff.pass.properties.php'
		);
	}

	public function testUppercaseConstants() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/UpperCaseConstantSniff.pass.uppercase.php'
		);
	}
}

