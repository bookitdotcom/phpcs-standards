<?php

class PreferShortOpenEchoSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.PHP.PreferShortOpenEcho';
	}

	public function testShortOpenEcho() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/PreferShortOpenEchoSniff.pass.php'
		);
	}

	public function testLongEcho() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/PreferShortOpenEchoSniff.Found.php',
			1,
			'Found'
		);
	}
}

