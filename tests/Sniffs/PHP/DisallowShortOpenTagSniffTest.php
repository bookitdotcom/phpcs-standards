<?php

class DisallowShortOpenTagSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.PHP.DisallowShortOpenTag';
	}

	public function testNoShortOpenTagsInPhp() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/DisallowShortOpenTagSniff.pass.php'
		);
	}

	public function testNoShortOpenTagsInPhtml() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/DisallowShortOpenTagSniff.pass.phtml'
		);
	}

	public function testShortOpenEchoFoundInPhp() {
		if (ini_get('short_open_tag') != 1 && PHP_MAJOR_VERSION >= 5 && PHP_MINOR_VERSION < 4) {
			$this->markTestSkipped('INI setting short_open_tag is turned Off; PHP file will not be parsed');
		}

		$this->assertSniffError(
			__DIR__ . '/_fixture/DisallowShortOpenTagSniff.EchoFound.php',
			1,
			'EchoFound'
		);
	}
	
	public function testShortOpenTagFoundInPhp() {
		if (ini_get('short_open_tag') != 1) {
			$this->markTestSkipped('INI setting short_open_tag is turned Off; PHP file will not be parsed');
		}

		$this->assertSniffError(
			__DIR__ . '/_fixture/DisallowShortOpenTagSniff.Found.php',
			1,
			'Found'
		);
	}
	
	public function testShortOpenTagFoundInPhtml() {
		if (ini_get('short_open_tag') != 1) {
			$this->markTestSkipped('INI setting short_open_tag is turned Off; PHP file will not be parsed');
		}

		$this->assertSniffError(
			__DIR__ . '/_fixture/DisallowShortOpenTagSniff.Found.phtml',
			1,
			'Found'
		);
	}
}

