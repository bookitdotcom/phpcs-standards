<?php

class AsperandSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.PHP.Asperand';
	}

	public function testAsperandFound() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/Asperand.Found.php',
			3,
			'Found'
		);
	}
}
