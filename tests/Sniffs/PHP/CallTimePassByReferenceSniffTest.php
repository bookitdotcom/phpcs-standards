<?php

class CallTimePassByReferenceSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.PHP.CallTimePassByReference';
	}

	public function testCallTimePassByReference() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/CallTimePassByReferenceSniff.NotAllowed.php',
			6,
			'NotAllowed'
		);
	}

	public function testParameterReferenceDeclaration() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/CallTimePassByReferenceSniff.pass.php'
		);
	}
}
