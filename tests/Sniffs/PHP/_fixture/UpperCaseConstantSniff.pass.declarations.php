<?php

namespace {

	$y = new \True\False\Null();

}

namespace True {
	interface ITrue {

	}
}

namespace True\False {

	use \True\ITrue as True;

	class False {

	}

	class Null extends False implements True {

	}

}

