<?php

function hello(&$var) {}
function goodbye($var) {}

$world = "foo";
hello($world);

$a = 0xAF;
$b = 0x9C;
echo $a & $b;

goodbye($a & $b);
goodbye(3 & $b);
goodbye(0x01 & $b);
