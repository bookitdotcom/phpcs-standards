<?php

class InterfaceNameSniffTest extends SniffTest {
	public static function getSniffName() {
		return 'BookIt.NamingConventions.InterfaceName';
	}

	public function testInterfaceNameWithCorrectNaming() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/InterfaceName.pass.php'
		);
	}

	public function testInterfaceNameWithoutLeadingI() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/InterfaceName.InterfaceBeginWithoutI.php',
			3,
			'InterfaceBeginWithoutI'
		);
	}
}
