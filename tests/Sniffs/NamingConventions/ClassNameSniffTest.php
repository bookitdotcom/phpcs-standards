<?php

class ClassNameSniffTest extends SniffTest {
	public static function getSniffName() {
		return 'BookIt.NamingConventions.ClassName';
	}

	public function testUpperCamelCase() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/ClassNameSniff.pass.php'
		);
	}

	public function testUnderscores() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ClassNameSniff.ClassNotUpperCamelCase.php',
			3,
			'ClassNotUpperCamelCase'
		);
	}

	public function testLowerCamelCase() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ClassNameSniff.ClassNotUpperCamelCase.php',
			7,
			'ClassNotUpperCamelCase'
		);
	}
}
