<?php

class VariableNameSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.NamingConventions.VariableName';
	}

	public function testClassMemberWithCorrectNaming() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/VariableName.pass.member.php'
		);
	}

	public function testClassMemberWithoutVariableScope() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/VariableName.NoVariableScope.php',
			5,
			'NoVariableScope'
		);
	}

	public function testPrivateClassMemberWithoutUnderscore() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/VariableName.PrivateUnderscore.php',
			7,
			'PrivateUnderscore'
		);
	}

	public function testProtectedClassMemberWithoutUnderscore() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/VariableName.PrivateUnderscore.php',
			5,
			'PrivateUnderscore'
		);
	}

	public function testPublicClassMemberWithUnderscore() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/VariableName.PublicUnderscore.php',
			5,
			'PublicUnderscore'
		);
	}

	public function testClassMemberWithUnderscoresInName() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/VariableName.ClassVariableUnderscore.php',
			5,
			'ClassVariableUnderscore'
		);
	}

	public function testVariableWithCorrectNaming() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/VariableName.pass.variable.php'
		);
	}

	public function testVariableBeginningWithUppercase() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/VariableName.UpperCaseVariable.php',
			3,
			'UpperCaseVariable'
		);
	}

	public function testVariableWithUnderscoresInName() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/VariableName.VariableUnderscore.php',
			3,
			'VariableUnderscore'
		);
	}
}
