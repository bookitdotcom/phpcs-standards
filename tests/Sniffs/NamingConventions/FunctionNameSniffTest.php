<?php

class FunctionNameSniffTest extends SniffTest {
	public static function getSniffName() {
		return 'BookIt.NamingConventions.FunctionName';
	}

	public function testClassMethodWithCorrectNaming() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/FunctionName.pass.scope.php'
		);
	}

	public function testClassMethodWithDoubleUnderscore() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/FunctionName.MethodDoubleUnderscore.php',
			4,
			'MethodDoubleUnderscore'
		);
	}

	public function testClassMethodWithUnderscores() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/FunctionName.MethodUnderscores.php',
			4,
			'MethodUnderscores'
		);
	}

	public function testClassMethodWithUpperCamelCaseName() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/FunctionName.MethodNotLowerCamelCase.php',
			4,
			'MethodNotLowerCamelCase'
		);
	}

	public function testFunctionWithCorrectNaming() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/FunctionName.pass.nonscope.php'
		);
	}

	public function testFunctionWithDoubleUnderscore() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/FunctionName.FunctionDoubleUnderscore.php',
			3,
			'FunctionDoubleUnderscore'
		);
	}

	public function testFunctionWithUnderscores() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/FunctionName.FunctionUnderscores.php',
			3,
			'FunctionUnderscores'
		);
	}

	public function testFunctionWithUpperCamelCaseName() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/FunctionName.FunctionNotLowerCamelCase.php',
			3,
			'FunctionNotLowerCamelCase'
		);
	}
}
