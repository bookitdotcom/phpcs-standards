<?php


class UpperCaseConstantNameSniff extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.NamingConventions.UpperCaseConstantName';
	}

	public function testUpperCaseConstantsPass() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/UpperCaseConstantName.pass.php'
		);
	}

	public function testLowerCaseConstantsUsingDefine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/UpperCaseConstantName.Found.php',
			'5', 'ConstantNotUpperCase'
		);
	}

	public function testLowerCaseConstantsUsingConst() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/UpperCaseConstantName.Found.php',
			'7', 'ClassConstantNotUpperCase'
		);
	}

	public function testLowerCaseClassConstants() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/UpperCaseConstantName.Found.php',
			'10', 'ClassConstantNotUpperCase'
		);
	}

	public function testLowerCaseConstantsUsingDefineInNamespace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/UpperCaseConstantName.Found.php',
			'16', 'ConstantNotUpperCase'
		);
	}

	public function testLowerCaseConstantsUsingConstInNamespace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/UpperCaseConstantName.Found.php',
			'18', 'ClassConstantNotUpperCase'
		);
	}

	public function testLowerCaseClassConstantsInNamespace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/UpperCaseConstantName.Found.php',
			'21', 'ClassConstantNotUpperCase'
		);
	}
} 