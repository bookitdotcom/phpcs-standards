<?php

namespace {
	define('FOO', 'foo');

	const BAR = 'bar';

	class Tests_Sniffs_NamingConventions_UpperCaseConstantNameSniffPass {
		const FOO = BAR;
	}
}

namespace Tests\Sniffs\NamingConventions {

	define('FOO', 'foo');

	const BAR = 'bar';

	class UpperCaseConstantNameSniffPass {
		const FOO = BAR;
	}
}
