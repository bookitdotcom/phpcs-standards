<?php

namespace {

	define('foo', 'foo');

	const bar = 'bar';

	class Tests_Sniffs_NamingConventions_UpperCaseConstantNameSniffFound {
		const foo = bar;
	}
}

namespace Tests\Sniffs\NamingConventions {

	define('foo', 'foo');

	const bar = 'bar';

	class UpperCaseConstantNameSniffFound {
		const foo = bar;
	}

}

