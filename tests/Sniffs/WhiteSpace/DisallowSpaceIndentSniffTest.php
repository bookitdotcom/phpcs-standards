<?php

class DisallowSpaceIndentSniffTest extends SniffTest {
	public static function getSniffName() {
		return 'BookIt.WhiteSpace.DisallowSpaceIndent';
	}

	public function testTabs() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/DisallowSpaceIndentSniff.pass.php'
		);
	}

	public function testSpaces() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/DisallowSpaceIndentSniff.SpacesUsedForIndent.php',
			4,
			'SpacesUsedForIndent'
		);
	}
}

