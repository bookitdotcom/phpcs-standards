<?php

namespace
{

	interface IBraces
	{
		public function foo();
	}

	class BracesImpl implements IBraces
	{
		public function foo()
		{
		}
	}

	function bar()
	{
	}

	$baz = function()
	{
	};

	if (TRUE)
	{
	}
	elseif (TRUE)
	{
	}
	else if (TRUE)
	{
	}
	else
	{
	}

	switch (TRUE)
	{
		default:
			break;
	}

	do
	{
	}
	while (FALSE);

	for ($i = 0; $i < 10; $i++)
	{
	}

	foreach ($i as $x)
	{
	}

	while (FALSE)
	{
	}

	try
	{
	}
	catch (Exception $e)
	{
	}

	declare (ticks = 1)
	{
	}

}
