<?php

namespace {

	interface IBraces {
		public function foo();
	} $x = 1;

	class BracesImpl implements IBraces {
		public function foo() {
		} public function bar() {
		}
	} $x = 1;

	function bar() {
	} $x = 1;

	if (FALSE) {
	} $x = 1;

	if (TRUE) {
	} elseif (FALSE) {
	} $x = 1;

	if (TRUE) {
	} else if (FALSE) {
	} $x = 1;

	if (TRUE) {
	} else {
	} $x = 1;

	switch (TRUE) {
		default:
			break;
	} $x = 1;

	for ($i = 0; $i < 10; $i++) {
	} $x = 1;

	foreach ($i as $x) {
	} $x = 1;

	while (FALSE) {
	} $x = 1;

	try {
	} catch (Exception $e) {
	} $x = 1;

	declare (ticks = 1) {
	} $x = 1;

} namespace Foo {

}
