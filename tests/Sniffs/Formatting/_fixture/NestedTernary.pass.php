<?php

$foo = TRUE ? TRUE : FALSE;

$foo = TRUE ?
	TRUE :
	FALSE;

$foo = TRUE
	? TRUE
	: FALSE;

$foo = TRUE
	?
	TRUE
	:
	FALSE;

array(
	TRUE ? : FALSE,
	TRUE ? : FALSE,
);

$b = TRUE ? 'foo' . 'bar' : FALSE;

$b = TRUE ? FALSE : 'FOO' . 'BAR';

$b = (TRUE ? : 'foo') . (TRUE ? : 'bar');