<?php

function foo($bar, $baz){
}

$foo1 = function($bar, $baz) use ($x){
};

$foo2 = function($bar, $baz){
};

if (TRUE){
} elseif (TRUE){
} else if (TRUE){
} else {
}

switch (TRUE){
	default:
		break;
}

for ($i = 0; $i < 1; $i++){
}

foreach ($foo as $bar){
}

while (FALSE){
}

try {
} catch (Exception $e){
}

declare (ticks = 1){
}
