<?php

$foo = TRUE ? TRUE ? TRUE : FALSE : FALSE;
$foo = TRUE ? TRUE : FALSE ? FALSE : TRUE;

$foo = TRUE ?
	TRUE ? TRUE : FALSE :
	FALSE;

$foo = TRUE ?
	TRUE :
	FALSE ? FALSE : TRUE;


$foo = TRUE
	? TRUE ? TRUE : FALSE
	: FALSE;

$foo = TRUE
	? TRUE
	: FALSE ? FALSE : TRUE;

$foo = TRUE
	?
	TRUE
		?
		TRUE
		:
		FALSE
	:
	FALSE;

$foo = TRUE
	?
	TRUE
	:
	FALSE
		?
		FALSE
		:
		TRUE;

array(
	TRUE ? : FALSE,
	TRUE ? : FALSE ? : TRUE,
	TRUE ? : FALSE ? : FALSE,
);

TRUE ? : TRUE ? array(1, 2) : FALSE;
TRUE ? TRUE ? array(1, 2) : FALSE : TRUE;

TRUE ? : TRUE ? : 'foo' . 'bar';
TRUE ? TRUE : TRUE ? : 'foo' . 'bar';