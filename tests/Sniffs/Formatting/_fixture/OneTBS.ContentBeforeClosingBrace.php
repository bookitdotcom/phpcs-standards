<?php

namespace Foo {}

namespace {

	interface IBraces {
		public function foo(); }

	class BracesImpl implements IBraces {
		public function foo() {}
		/* end */ }

	function bar() {}

	if (TRUE) {
		$x = 1; } elseif (TRUE) {
		$x = 1; } else if (TRUE) {
		$x = 1; } else {
		$x = 1; }

	switch (TRUE) {
		default:
			break; }

	do {
		$x = 1; } while (FALSE);

	for ($i = 0; $i < 10; $i++) {}

	foreach ($i as $x) {}

	while (FALSE) {}

	try {
		$x = 1; } catch (Exception $e) {
		$x = 1;	}

	declare (ticks = 1) {}

}
