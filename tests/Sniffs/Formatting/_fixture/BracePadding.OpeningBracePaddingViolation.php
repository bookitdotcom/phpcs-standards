<?php

namespace Foo{

	interface IBar{
		public function doTheBender();
	}

	class Baz implements IBar{

		public function doTheBender(){
		}

	}
}

namespace {

	function doTheBender(){
	}

	$anonymous = function(){
	};

	if (TRUE){
	} elseif (TRUE){
	} else if (TRUE){
	} else{
	}

	switch (TRUE){
		default;
			break;
	}

	do{
	} while (FALSE);

	for ($i = 0; $i < 10; $i++){
	}

	foreach ($i as $bar){
	}

	while (FALSE){
	}

	try{
	} catch (\Exception $e){
	}

	declare (ticks = 1){
	}

}
