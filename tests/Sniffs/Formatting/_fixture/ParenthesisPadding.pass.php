<?php

use RuntimeException as E;

function foo($bar, $baz) {
}

$foo = function($bar, $baz) use ($x) {
};

if (TRUE) {
} elseif (TRUE) {
} else if (TRUE) {
} else {
}

switch (TRUE) {
	default:
		break;
}

for ($i = 0; $i < 1; $i++) {
}

foreach ($foo as $bar) {
}

while (FALSE) {
}

do {
} while (FALSE);

try {
} catch (Exception $e) {
}

declare (ticks = 1) {
}
