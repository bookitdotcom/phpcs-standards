<?php

class NestedTernarySniff extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Formatting.NestedTernary';
	}

	public function testNoErrors() {
		$this->assertSniffPasses(__DIR__ . '/_fixture/NestedTernary.pass.php');
	}

	public function testNestedOnSingleLineThrowsError() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/NestedTernary.Found.php',
			3,
			'NestedTernary'
		);

		$this->assertSniffError(
			__DIR__ . '/_fixture/NestedTernary.Found.php',
			4,
			'NestedTernary'
		);
	}

	public function testNestedOnMultiLineThrowsError() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/NestedTernary.Found.php',
			7,
			'NestedTernary'
		);

		$this->assertSniffError(
			__DIR__ . '/_fixture/NestedTernary.Found.php',
			12,
			'NestedTernary'
		);

		$this->assertSniffError(
			__DIR__ . '/_fixture/NestedTernary.Found.php',
			16,
			'NestedTernary'
		);

		$this->assertSniffError(
			__DIR__ . '/_fixture/NestedTernary.Found.php',
			21,
			'NestedTernary'
		);

		$this->assertSniffError(
			__DIR__ . '/_fixture/NestedTernary.Found.php',
			26,
			'NestedTernary'
		);

		$this->assertSniffError(
			__DIR__ . '/_fixture/NestedTernary.Found.php',
			38,
			'NestedTernary'
		);
	}
}
