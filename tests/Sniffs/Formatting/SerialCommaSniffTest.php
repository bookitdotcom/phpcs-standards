<?php

class SerialCommaSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Formatting.SerialComma';
	}

	public function testSerialComma() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/SerialComma.pass.php'
		);
	}

	public function testMissingLastComma() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/SerialComma.MissingLastComma.php',
			6,
			'MissingLastComma'
		);
	}
}
