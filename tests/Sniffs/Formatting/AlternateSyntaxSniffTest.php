<?php

class AlternateSyntaxSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Formatting.AlternateSyntax';
	}

	public function testTraditionalSyntax() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/AlternateSyntax.pass.php'
		);
	}

	public function testIfAlternateSyntax() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/AlternateSyntax.AlternateSyntax.php',
			3,
			'AlternateSyntax'
		);
	}

	public function testElseIfAlternateSyntax() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/AlternateSyntax.AlternateSyntax.php',
			4,
			'AlternateSyntax'
		);
	}

	public function testElseAlternateSyntax() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/AlternateSyntax.AlternateSyntax.php',
			5,
			'AlternateSyntax'
		);
	}

	public function testSwitchAlternateSyntax() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/AlternateSyntax.AlternateSyntax.php',
			8,
			'AlternateSyntax'
		);
	}

	public function testWhileAlternateSyntax() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/AlternateSyntax.AlternateSyntax.php',
			13,
			'AlternateSyntax'
		);
	}

	public function testForAlternateSyntax() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/AlternateSyntax.AlternateSyntax.php',
			16,
			'AlternateSyntax'
		);
	}

	public function testForeachAlternateSyntax() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/AlternateSyntax.AlternateSyntax.php',
			19,
			'AlternateSyntax'
		);
	}
}
