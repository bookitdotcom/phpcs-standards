<?php

class OneTBSSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Formatting.OneTBS';
	}

	public function testCorrectBraceStyle() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/OneTBS.pass.php'
		);
	}

	public function testIfMissingOpeningBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.MissingOpeningBrace.php',
			3,
			'MissingOpeningBrace'
		);
	}

	public function testCombinedElseIfMissingOpeningBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.MissingOpeningBrace.php',
			5,
			'MissingOpeningBrace'
		);
	}

	public function testSplitElseIfMissingOpeningBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.MissingOpeningBrace.php',
			7,
			'MissingOpeningBrace'
		);
	}

	public function testElseMissingOpeningBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.MissingOpeningBrace.php',
			9,
			'MissingOpeningBrace'
		);
	}

	public function testForMissingOpeningBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.MissingOpeningBrace.php',
			12,
			'MissingOpeningBrace'
		);
	}

	public function testForeachMissingOpeningBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.MissingOpeningBrace.php',
			15,
			'MissingOpeningBrace'
		);
	}

	public function testWhileMissingOpeningBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.MissingOpeningBrace.php',
			18,
			'MissingOpeningBrace'
		);
	}

	public function testNamespaceWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			4,
			'OpenBraceViolation'
		);
	}

	public function testInterfaceWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			7,
			'OpenBraceViolation'
		);
	}

	public function testClassWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			12,
			'OpenBraceViolation'
		);
	}

	public function testClassMethodWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			14,
			'OpenBraceViolation'
		);
	}

	public function testFunctionWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			19,
			'OpenBraceViolation'
		);
	}

	public function testClosureWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			23,
			'OpenBraceViolation'
		);
	}

	public function testIfWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			27,
			'OpenBraceViolation'
		);
	}

	public function testCombinedElseIfWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			30,
			'OpenBraceViolation'
		);
	}

	public function testSplitElseIfWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			33,
			'OpenBraceViolation'
		);
	}

	public function testElseWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			36,
			'OpenBraceViolation'
		);
	}

	public function testSwitchWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			40,
			'OpenBraceViolation'
		);
	}

	public function testDoWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			46,
			'OpenBraceViolation'
		);
	}

	public function testForWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			51,
			'OpenBraceViolation'
		);
	}

	public function testForeachWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			55,
			'OpenBraceViolation'
		);
	}

	public function testWhileWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			59,
			'OpenBraceViolation'
		);
	}

	public function testTryWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			63,
			'OpenBraceViolation'
		);
	}

	public function testCatchWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			66,
			'OpenBraceViolation'
		);
	}

	public function testDeclareWithBraceOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.OpenBraceViolation.php',
			70,
			'OpenBraceViolation'
		);
	}

	public function testIfWithNextOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.CloseBraceViolation.php',
			4,
			'CloseBraceViolation'
		);
	}

	public function testCombinedElseIfWithNextOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.CloseBraceViolation.php',
			6,
			'CloseBraceViolation'
		);
	}

	public function testSplitElseIfWithNextOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.CloseBraceViolation.php',
			8,
			'CloseBraceViolation'
		);
	}

	public function testDoWithWhileOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.CloseBraceViolation.php',
			13,
			'CloseBraceViolation'
		);
	}

	public function testTryWithCatchOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.CloseBraceViolation.php',
			17,
			'CloseBraceViolation'
		);
	}

	public function testCatchWithCatchOnNextLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.CloseBraceViolation.php',
			19,
			'CloseBraceViolation'
		);
	}

	public function testInterfaceWithContentAfterClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentAfterClosingBrace.php',
			7,
			'ContentAfterClosingBrace'
		);
	}

	public function testClassMethodWithContentAfterClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentAfterClosingBrace.php',
			11,
			'ContentAfterClosingBrace'
		);
	}

	public function testClassWithContentAfterClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentAfterClosingBrace.php',
			13,
			'ContentAfterClosingBrace'
		);
	}

	public function testFunctionWithContentAfterClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentAfterClosingBrace.php',
			16,
			'ContentAfterClosingBrace'
		);
	}

	public function testIfWithContentAfterClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentAfterClosingBrace.php',
			19,
			'ContentAfterClosingBrace'
		);
	}

	public function testCombinedElseIfWithContentAfterClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentAfterClosingBrace.php',
			23,
			'ContentAfterClosingBrace'
		);
	}

	public function testSplitElseIfWithContentAfterClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentAfterClosingBrace.php',
			27,
			'ContentAfterClosingBrace'
		);
	}

	public function testElseWithContentAfterClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentAfterClosingBrace.php',
			31,
			'ContentAfterClosingBrace'
		);
	}

	public function testSwitchWithContentAfterClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentAfterClosingBrace.php',
			36,
			'ContentAfterClosingBrace'
		);
	}

	public function testForWithContentAfterClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentAfterClosingBrace.php',
			39,
			'ContentAfterClosingBrace'
		);
	}

	public function testForeachWithContentAfterClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentAfterClosingBrace.php',
			42,
			'ContentAfterClosingBrace'
		);
	}

	public function testWhileWithContentAfterClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentAfterClosingBrace.php',
			45,
			'ContentAfterClosingBrace'
		);
	}

	public function testCatchWithContentAfterClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentAfterClosingBrace.php',
			49,
			'ContentAfterClosingBrace'
		);
	}

	public function testDeclareWithContentAfterClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentAfterClosingBrace.php',
			52,
			'ContentAfterClosingBrace'
		);
	}

	public function testNamespaceWithContentAfterClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentAfterClosingBrace.php',
			54,
			'ContentAfterClosingBrace'
		);
	}

	public function testNamespaceWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			3,
			'ContentBeforeClosingBrace'
		);
	}

	public function testInterfaceWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			8,
			'ContentBeforeClosingBrace'
		);
	}

	public function testClassMethodWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			11,
			'ContentBeforeClosingBrace'
		);
	}

	public function testClassWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			12,
			'ContentBeforeClosingBrace'
		);
	}

	public function testFunctionWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			14,
			'ContentBeforeClosingBrace'
		);
	}

	public function testIfWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			17,
			'ContentBeforeClosingBrace'
		);
	}

	public function testCombinedElseIfWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			18,
			'ContentBeforeClosingBrace'
		);
	}

	public function testSplitElseIfWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			19,
			'ContentBeforeClosingBrace'
		);
	}

	public function testElseWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			20,
			'ContentBeforeClosingBrace'
		);
	}

	public function testSwitchWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			24,
			'ContentBeforeClosingBrace'
		);
	}

	public function testDoWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			27,
			'ContentBeforeClosingBrace'
		);
	}

	public function testForWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			29,
			'ContentBeforeClosingBrace'
		);
	}

	public function testForeachWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			31,
			'ContentBeforeClosingBrace'
		);
	}

	public function testWhileWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			33,
			'ContentBeforeClosingBrace'
		);
	}

	public function testTryWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			36,
			'ContentBeforeClosingBrace'
		);
	}

	public function testCatchWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			37,
			'ContentBeforeClosingBrace'
		);
	}

	public function testDeclareWithContentBeforeClosingBrace() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/OneTBS.ContentBeforeClosingBrace.php',
			39,
			'ContentBeforeClosingBrace'
		);
	}
}
