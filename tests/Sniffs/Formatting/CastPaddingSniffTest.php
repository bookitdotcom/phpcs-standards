<?php

class CastPaddingSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Formatting.CastPadding';
	}

	public function testCorrectPadding() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/CastPadding.pass.php'
		);
	}

	public function testArrayCastWithoutPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/CastPadding.CastPaddingViolation.php',
			3,
			'CastPaddingViolation'
		);
	}

	public function testBooleanCastWithoutPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/CastPadding.CastPaddingViolation.php',
			5,
			'CastPaddingViolation'
		);
	}

	public function testBoolCastWithoutPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/CastPadding.CastPaddingViolation.php',
			6,
			'CastPaddingViolation'
		);
	}

	public function testRealCastWithoutPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/CastPadding.CastPaddingViolation.php',
			8,
			'CastPaddingViolation'
		);
	}

	public function testDoubleCastWithoutPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/CastPadding.CastPaddingViolation.php',
			9,
			'CastPaddingViolation'
		);
	}

	public function testFloatCastWithoutPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/CastPadding.CastPaddingViolation.php',
			10,
			'CastPaddingViolation'
		);
	}

	public function testIntCastWithoutPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/CastPadding.CastPaddingViolation.php',
			12,
			'CastPaddingViolation'
		);
	}

	public function testIntegerCastWithoutPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/CastPadding.CastPaddingViolation.php',
			13,
			'CastPaddingViolation'
		);
	}

	public function testObjectCastWithoutPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/CastPadding.CastPaddingViolation.php',
			15,
			'CastPaddingViolation'
		);
	}

	public function testStringCastWithoutPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/CastPadding.CastPaddingViolation.php',
			17,
			'CastPaddingViolation'
		);
	}

	public function testUnsetCastWithoutPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/CastPadding.CastPaddingViolation.php',
			19,
			'CastPaddingViolation'
		);
	}
}
