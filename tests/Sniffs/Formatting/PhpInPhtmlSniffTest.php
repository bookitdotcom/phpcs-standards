<?php

class PhpInPhtmlSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Formatting.PhpInPhtml';
	}

	public function testPhpTagsExceptEchoOnSeparateLines() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/PhpInPhtml.pass.phtml'
		);
	}

	public function testPhpTagsOnSameLine() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/PhpInPhtml.PhpTagsInline.phtml',
			1,
			'PhpTagsInline'
		);
		$this->assertSniffError(
			__DIR__ . '/_fixture/PhpInPhtml.PhpTagsInline.phtml',
			3,
			'PhpTagsInline'
		);
	}

}
