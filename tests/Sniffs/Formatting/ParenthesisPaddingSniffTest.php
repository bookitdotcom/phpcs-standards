<?php

class ParenthesisPaddingSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Formatting.ParenthesisPadding';
	}

	public function testCorrectPadding() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/ParenthesisPadding.pass.php'
		);
	}

	public function testClosureUseWithoutOpenParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.OpeningParenPaddingViolation.php',
			3,
			'OpeningParenPaddingViolation'
		);
	}

	public function testIfWithoutOpenParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.OpeningParenPaddingViolation.php',
			6,
			'OpeningParenPaddingViolation'
		);
	}

	public function testCombinedElseIfWithoutOpenParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.OpeningParenPaddingViolation.php',
			7,
			'OpeningParenPaddingViolation'
		);
	}

	public function testSplitElseIfWithoutOpenParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.OpeningParenPaddingViolation.php',
			8,
			'OpeningParenPaddingViolation'
		);
	}

	public function testSwitchWithoutOpenParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.OpeningParenPaddingViolation.php',
			12,
			'OpeningParenPaddingViolation'
		);
	}

	public function testForWithoutOpenParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.OpeningParenPaddingViolation.php',
			17,
			'OpeningParenPaddingViolation'
		);
	}

	public function testForeachWithoutOpenParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.OpeningParenPaddingViolation.php',
			20,
			'OpeningParenPaddingViolation'
		);
	}

	public function testWhileWithoutOpenParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.OpeningParenPaddingViolation.php',
			23,
			'OpeningParenPaddingViolation'
		);
	}

	public function testCatchWithoutOpenParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.OpeningParenPaddingViolation.php',
			27,
			'OpeningParenPaddingViolation'
		);
	}

	public function testDeclareWithoutOpenParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.OpeningParenPaddingViolation.php',
			30,
			'OpeningParenPaddingViolation'
		);
	}

	public function testFunctionWithoutClosingParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.ClosingParenPaddingViolation.php',
			3,
			'ClosingParenPaddingViolation'
		);
	}

	public function testClosureUseWithoutClosingParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.ClosingParenPaddingViolation.php',
			6,
			'ClosingParenPaddingViolation'
		);
	}

	public function testClosureWithoutClosingParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.ClosingParenPaddingViolation.php',
			9,
			'ClosingParenPaddingViolation'
		);
	}

	public function testIfWithoutClosingParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.ClosingParenPaddingViolation.php',
			12,
			'ClosingParenPaddingViolation'
		);
	}

	public function testCombinedElseIfWithoutClosingParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.ClosingParenPaddingViolation.php',
			13,
			'ClosingParenPaddingViolation'
		);
	}

	public function testSplitElseIfWithoutClosingParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.ClosingParenPaddingViolation.php',
			14,
			'ClosingParenPaddingViolation'
		);
	}

	public function testSwitchWithoutClosingParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.ClosingParenPaddingViolation.php',
			18,
			'ClosingParenPaddingViolation'
		);
	}

	public function testForWithoutClosingParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.ClosingParenPaddingViolation.php',
			23,
			'ClosingParenPaddingViolation'
		);
	}

	public function testForeachWithoutClosingParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.ClosingParenPaddingViolation.php',
			26,
			'ClosingParenPaddingViolation'
		);
	}

	public function testWhileWithoutClosingParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.ClosingParenPaddingViolation.php',
			29,
			'ClosingParenPaddingViolation'
		);
	}

	public function testCatchWithoutClosingParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.ClosingParenPaddingViolation.php',
			33,
			'ClosingParenPaddingViolation'
		);
	}

	public function testDeclareWithoutClosingParenPadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ParenthesisPadding.ClosingParenPaddingViolation.php',
			36,
			'ClosingParenPaddingViolation'
		);
	}
}
