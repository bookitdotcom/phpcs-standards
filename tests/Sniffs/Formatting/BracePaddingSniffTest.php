<?php

class BracePaddingSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Formatting.BracePadding';
	}

	public function testCorrectPadding() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/BracePadding.pass.php'
		);
	}

	public function testNamespaceWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			3,
			'OpeningBracePaddingViolation'
		);
	}

	public function testInterfaceWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			5,
			'OpeningBracePaddingViolation'
		);
	}

	public function testClassWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			9,
			'OpeningBracePaddingViolation'
		);
	}

	public function testClassMethodWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			11,
			'OpeningBracePaddingViolation'
		);
	}

	public function testFunctionWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			19,
			'OpeningBracePaddingViolation'
		);
	}

	public function testClosureWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			22,
			'OpeningBracePaddingViolation'
		);
	}

	public function testIfWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			25,
			'OpeningBracePaddingViolation'
		);
	}

	public function testCombinedElseIfWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			26,
			'OpeningBracePaddingViolation'
		);
	}

	public function testSplitElseIfWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			27,
			'OpeningBracePaddingViolation'
		);
	}

	public function testElseWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			28,
			'OpeningBracePaddingViolation'
		);
	}

	public function testSwitchWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			31,
			'OpeningBracePaddingViolation'
		);
	}

	public function testDoWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			36,
			'OpeningBracePaddingViolation'
		);
	}

	public function testForWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			39,
			'OpeningBracePaddingViolation'
		);
	}

	public function testForeachWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			42,
			'OpeningBracePaddingViolation'
		);
	}

	public function testWhileWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			45,
			'OpeningBracePaddingViolation'
		);
	}

	public function testTryWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			48,
			'OpeningBracePaddingViolation'
		);
	}

	public function testCatchWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			49,
			'OpeningBracePaddingViolation'
		);
	}

	public function testDeclareWithoutOpeningBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.OpeningBracePaddingViolation.php',
			52,
			'OpeningBracePaddingViolation'
		);
	}

	public function testIfWithoutClosingBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.ClosingBracePaddingViolation.php',
			4,
			'ClosingBracePaddingViolation'
		);
	}

	public function testCombinedElseIfWithoutClosingBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.ClosingBracePaddingViolation.php',
			5,
			'ClosingBracePaddingViolation'
		);
	}

	public function testSplitElseIfWithoutClosingBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.ClosingBracePaddingViolation.php',
			6,
			'ClosingBracePaddingViolation'
		);
	}

	public function testDoWithoutClosingBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.ClosingBracePaddingViolation.php',
			10,
			'ClosingBracePaddingViolation'
		);
	}

	public function testTryWithoutClosingBracePadding() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/BracePadding.ClosingBracePaddingViolation.php',
			13,
			'ClosingBracePaddingViolation'
		);
	}
}
