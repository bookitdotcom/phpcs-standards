<?php

class HashCommentSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Commenting.HashComment';
	}

	public function testCorrectComments() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/HashComment.pass.php'
		);
	}

	public function testHashComment() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/HashComment.Found.php',
			3,
			'Found'
		);
	}
}
