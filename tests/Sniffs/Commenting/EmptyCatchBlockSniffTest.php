<?php

class EmptyCatchBlockSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Commenting.EmptyCatchBlock';
	}

	public function testNonEmptyCatchBlocks() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/EmptyCatchBlock.pass.php'
		);
	}

	public function testEmptyCatchBlocks() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/EmptyCatchBlock.ExplanationRequired.php',
			5,
			'ExplanationRequired'
		);
	}

}
