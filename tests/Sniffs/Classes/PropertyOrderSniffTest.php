<?php

class PropertyOrderSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Classes.PropertyOrder';
	}

	public function testCorrectPropertyOrder() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/PropertyOrder.pass.php'
		);
	}

	public function testPublicThenPrivateThenProtected() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/PropertyOrder.ClassPropertiesOutOfOrder.php',
			9,
			'ClassPropertiesOutOfOrder',
			'protected properties must be defined before private properties'
		);
	}

	public function testProtectedThenPublic() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/PropertyOrder.ClassPropertiesOutOfOrder.php',
			16,
			'ClassPropertiesOutOfOrder',
			'public properties must be defined before protected, private properties'
		);
	}

	public function testPrivateThenPublic() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/PropertyOrder.ClassPropertiesOutOfOrder.php',
			23,
			'ClassPropertiesOutOfOrder',
			'public properties must be defined before private properties'
		);
	}

	public function testPrivateThenProtectedThenPublic() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/PropertyOrder.ClassPropertiesOutOfOrder.php',
			30,
			'ClassPropertiesOutOfOrder',
			'protected properties must be defined before private properties'
		);
		$this->assertSniffError(
			__DIR__ . '/_fixture/PropertyOrder.ClassPropertiesOutOfOrder.php',
			32,
			'ClassPropertiesOutOfOrder',
			'public properties must be defined before private properties'
		);
	}
}
