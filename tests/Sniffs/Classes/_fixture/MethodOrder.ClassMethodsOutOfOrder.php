<?php

class PubPrivProt {

	public function foo() {
	}

	private function baz() {
	}

	protected function bar() {
	}
}

class ProtPub {

	protected function bar() {
	}

	public function foo() {
	}
}

class PrivPub {

	private function baz() {
	}

	public function foo() {
	}
}

class PrivProtPub {

	private function baz() {
	}

	protected function bar() {
	}

	public function foo() {
	}
}
