<?php

class CorrectMethodOrderA {

	public function foo() {

	}

	protected function bar() {

	}

	private function baz() {

	}
}

class CorrectMethodOrderB {

	public function foo() {

	}

	protected function bar() {

	}
}

class CorrectMethodOrderC {

	public function foo() {

	}
}

class CorrectMethodOrderD {

	public function foo() {

	}

	private function baz() {

	}
}

class CorrectMethodOrderE {

	protected function bar() {

	}

	private function baz() {

	}
}

class CorrectMethodOrderF {

	protected function bar() {

	}
}

class CorrectMethodOrderG {

	private function baz() {

	}
}
