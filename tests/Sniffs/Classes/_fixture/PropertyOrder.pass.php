<?php

class CorrectPropertyOrderA {

	public $foo = 'foo';

	protected $_bar = 'bar';

	private $_baz = 'baz';

	// method params show as properties ;)
	public function foo($bar) {
		return $this;
	}
}

class CorrectPropertyOrderB {

	public $foo = 'foo';

	protected $_bar = 'bar';
}

class CorrectPropertyOrderC {

	public $foo = 'foo';
}

class CorrectPropertyOrderD {

	public $foo = 'foo';

	private $_baz = 'baz';
}

class CorrectPropertyOrderE {

	protected $_bar = 'bar';

	private $_baz = 'baz';
}

class CorrectPropertyOrderF {

	protected $_bar = 'bar';
}

class CorrectPropertyOrderG {

	private $_baz = 'baz';
}
