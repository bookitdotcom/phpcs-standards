<?php

class PubPrivProt {

	public $foo = 'foo';

	private  $_baz = 'baz';

	protected  $_bar = 'bar';
}

class ProtPub {

	protected  $_bar = 'bar';

	public $foo = 'foo';
}

class PrivPub {

	private  $_baz = 'baz';

	public $foo = 'foo';
}

class PrivProtPub {

	private  $_baz = 'baz';

	protected  $_bar = 'bar';

	public $foo = 'foo';
}
