<?php

class ConMethProp {

	const CLASS_CONSTANT = 'foo';

	public function foo() {

	}

	public $bar = 'bar';

}

class PropCon {

	public $bar = 'bar';

	const CLASS_CONSTANT = 'foo';

}

class MethCon {

	public function foo() {

	}

	const CLASS_CONSTANT = 'foo';

}

class MethPropCon {

	public function foo() {

	}

	public $bar = 'bar';

	const CLASS_CONSTANT = 'foo';

}
