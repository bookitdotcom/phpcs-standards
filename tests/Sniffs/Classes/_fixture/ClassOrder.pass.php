<?php

class CorrectlyOrderedA {

	const CLASS_CONSTANT = 'foo';

	public $foo = 'foo';

	protected $_bar = 'bar';

	private $_baz = 'baz';

	public function foo() {

	}

	protected function bar() {

	}

	private function baz() {

	}
}

class CorrectlyOrderedB {

	public $foo = 'foo';

	protected $_bar = 'bar';

	private $_baz = 'baz';

	public function foo() {

	}

	protected function bar() {

	}

	private function baz() {

	}
}

class CorrectlyOrderedC {

	public function foo() {

	}

	protected function bar() {

	}

	private function baz() {

	}
}

class CorrectlyOrderedD {

	const CLASS_CONSTANT = 'foo';

	public function foo() {

	}

	protected function bar() {

	}

	private function baz() {

	}
}

class CorrectlyOrderedE {

	const CLASS_CONSTANT = 'foo';

}

class CorrectlyOrderedF {

	const CLASS_CONSTANT = 'foo';

	public $foo = 'foo';

	protected $_bar = 'bar';

	private $_baz = 'baz';

}

class CorrectlyOrderedG {

	public $foo = 'foo';

	protected $_bar = 'bar';

	private $_baz = 'baz';

}
