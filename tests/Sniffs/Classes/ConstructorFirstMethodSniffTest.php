<?php

class ConstructorFirstMethodSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Classes.ConstructorFirstMethod';
	}

	public function testConstructorIsFirstMethod() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/ConstructorFirstMethod.pass.php'
		);
	}

	public function testConstructorNotFirstMethod() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ConstructorFirstMethod.ConstructorNotFirst.php',
			9,
			'ConstructorNotFirst'
		);
	}
}
