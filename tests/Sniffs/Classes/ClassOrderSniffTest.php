<?php

class ClassOrderSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Classes.ClassOrder';
	}

	public function testCorrectClassOrder() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/ClassOrder.pass.php'
		);
	}

	public function testConstantsThenMethodsThenProperties() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ClassOrder.ClassOutOfOrder.php',
			11,
			'ClassOutOfOrder',
			'Class variable is not allowed here; must come before methods'
		);
	}

	public function testPropertiesThenConstants() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ClassOrder.ClassOutOfOrder.php',
			19,
			'ClassOutOfOrder',
			'Class constant is not allowed here; must come before variables, methods'
		);
	}

	public function testMethodsThenConstants() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ClassOrder.ClassOutOfOrder.php',
			29,
			'ClassOutOfOrder',
			'Class constant is not allowed here; must come before methods'
		);
	}

	public function testMethodsThenPropertiesThenConstants() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/ClassOrder.ClassOutOfOrder.php',
			39,
			'ClassOutOfOrder',
			'Class variable is not allowed here; must come before methods'
		);
		$this->assertSniffError(
			__DIR__ . '/_fixture/ClassOrder.ClassOutOfOrder.php',
			41,
			'ClassOutOfOrder',
			'Class constant is not allowed here; must come before methods'
		);
	}
}
