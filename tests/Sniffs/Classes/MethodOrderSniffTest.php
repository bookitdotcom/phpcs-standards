<?php

class MethodOrderSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Classes.MethodOrder';
	}

	public function testCorrectMethodOrder() {
		$this->assertSniffPasses(
			__DIR__ . '/_fixture/MethodOrder.pass.php'
		);
	}

	public function testPublicThenPrivateThenProtected() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/MethodOrder.ClassMethodsOutOfOrder.php',
			11,
			'ClassMethodsOutOfOrder',
			'protected methods must be defined before private methods'
		);
	}

	public function testProtectedThenPublic() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/MethodOrder.ClassMethodsOutOfOrder.php',
			20,
			'ClassMethodsOutOfOrder',
			'public methods must be defined before protected, private methods'
		);
	}

	public function testPrivateThenPublic() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/MethodOrder.ClassMethodsOutOfOrder.php',
			29,
			'ClassMethodsOutOfOrder',
			'public methods must be defined before private methods'
		);
	}

	public function testPrivateThenProtectedThenPublic() {
		$this->assertSniffError(
			__DIR__ . '/_fixture/MethodOrder.ClassMethodsOutOfOrder.php',
			38,
			'ClassMethodsOutOfOrder',
			'protected methods must be defined before private methods'
		);
		$this->assertSniffError(
			__DIR__ . '/_fixture/MethodOrder.ClassMethodsOutOfOrder.php',
			41,
			'ClassMethodsOutOfOrder',
			'public methods must be defined before private methods'
		);
	}
}
