<?php

class ArrayFunctionsSniffTest extends SniffTest {

	public static function getSniffName() {
		return 'BookIt.Performance.ArrayFunctions';
	}

	public function testAllowedFunctions() {
		$this->assertSniffPasses(__DIR__. '/_fixture/ArrayFunctions.pass.php');
	}

	public function testUseArrayFilter() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/ArrayFunctions.InefficientFunction.php',
			5,
			'InefficientFunction'
		);
	}

	public function testUseInArray() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/ArrayFunctions.InefficientFunction.php',
			6,
			'InefficientFunction'
		);
	}

	public function testUseArrayDiff() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/ArrayFunctions.InefficientFunction.php',
			7,
			'InefficientFunction'
		);
	}

	public function testUseArrayKeyExists() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/ArrayFunctions.InefficientFunction.php',
			8,
			'InefficientFunction'
		);
	}

	public function testUseArrayMerge() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/ArrayFunctions.InefficientFunction.php',
			9,
			'InefficientFunction'
		);
	}

	public function testUseArrayMap() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/ArrayFunctions.InefficientFunction.php',
			10,
			'InefficientFunction'
		);
	}

	public function testUseArrayUnique() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/ArrayFunctions.InefficientFunction.php',
			11,
			'InefficientFunction'
		);
	}

	public function testUseEnd() {
		$this->assertSniffWarning(
			__DIR__ . '/_fixture/ArrayFunctions.InefficientFunction.php',
			12,
			'InefficientFunction'
		);
	}
}
