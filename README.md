BookIt PHPCS Standards
======================

Dependencies
------------

These standards are essentially a very complex form of configuration.  You need PHP CodeSniffer in order to actually inspect code against these standards.

To install PHP CodeSniffer with PEAR:

    sudo pear config-set auto_discover 1
    sudo pear install --alldeps pear.php.net/PHP_CodeSniffer

To install PHP CodeSniffer with Pyrus:

    sudo pyrus set auto_discover 1
    sudo pyrus install --optionaldeps pear.php.net/PHP_CodeSniffer

In addition, PHP CodeSniffer is defined as a development dependency, along with PHPUnit.  It is required for functional testing of the standards.  You
could use the PHP CodeSniffer binary included in the dev dependencies.

Using the Standards
-------------------

Run against a single file:

    phpcs --standard=/path/to/phpcs-bookit-standard.git/BookIt /path/to/your/file.php

Run against a directory:

    phpcs --standard=/path/to/phpcs-bookit-standard.git/BookIt /path/to/your/directory

List standards:

    phpcs --standard=/path/to/phpcs-bookit-standard.git/BookIt -e

Running Functional Tests
------------------------

1. Run `composer install`.  This will install PHP_CodeSniffer and PHPUnit for testing, as well as generate a classmap and autoloader for testing.

2. Run `vendor/bin/phpunit` from the project root directory.

