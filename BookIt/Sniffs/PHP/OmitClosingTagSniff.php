<?php

namespace BookIt\Sniffs\PHP;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class OmitClosingTagSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_CLOSE_TAG);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		if (!isset($tokens[$stackPtr + 1])) {
			$error = 'Closing ?> tag must be omitted';
			$phpcsFile->addError($error, $stackPtr, 'CloseTag');
		}
	}
}
