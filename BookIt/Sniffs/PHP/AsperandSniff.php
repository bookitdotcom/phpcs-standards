<?php

namespace BookIt\Sniffs\PHP;

use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_Tokens;

class AsperandSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_ASPERAND);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		$function = $phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, $stackPtr + 1, NULL, TRUE);;

		$phpcsFile->addError(
			'Do not use error suppression; expected "%s()" but found "@%s()"',
			$stackPtr,
			'Found',
			array(
				$tokens[$function]['content'],
				$tokens[$function]['content'],
			)
		);
	}
}
