<?php

namespace BookIt\Sniffs\PHP;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Tokens;

class DisallowShortOpenTagSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(
			T_OPEN_TAG,
			T_OPEN_TAG_WITH_ECHO,
		);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens  = $phpcsFile->getTokens();
		$openTag = $tokens[$stackPtr];

		// No short open tag
		if ($openTag['content'] === '<?') {
			$error = 'Short PHP opening tag used; expected "<?php" but found "%s"';
			$data  = array($openTag['content']);
			$phpcsFile->addError($error, $stackPtr, 'Found', $data);
		}

		// Open with echo tag only allowed in PHTML files
		if ($openTag['code'] === T_OPEN_TAG_WITH_ECHO && !preg_match('/\.phtml$/', $phpcsFile->getFileName())) {
			$nextVar = $tokens[$phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, ($stackPtr + 1), NULL, TRUE)];

			$error = 'Short PHP opening tag with echo only permitted in PHTML file; found "%s %s ..."';
			$data = array(
				$openTag['content'],
				$nextVar['content'],
			);
			$phpcsFile->addError($error, $stackPtr, 'EchoFound', $data);
		}
	}

}
