<?php

namespace BookIt\Sniffs\PHP;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Tokens;

class PreferShortOpenEchoSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_OPEN_TAG);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens  = $phpcsFile->getTokens();
		$openTag = $tokens[$stackPtr];
		$nextVar = $tokens[$phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, ($stackPtr + 1), NULL, TRUE)];
		$echoStr = $tokens[$phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, ($stackPtr + 2), NULL, TRUE)];

		if ($nextVar['content'] === 'echo') {
			$warning = 'Short PHP opening tag with echo preferred; expected "<?= %s ..." but found "%s %s %s ..."';
			$data = array(
				$echoStr['content'],
				$openTag['content'],
				$nextVar['content'],
				$echoStr['content'],
			);
			$phpcsFile->addWarning($warning, $stackPtr, 'Found', $data);
		}
	}
}
