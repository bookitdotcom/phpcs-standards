<?php

namespace BookIt\Sniffs\PHP;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class UpperCaseConstantSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_TRUE, T_FALSE, T_NULL);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens  = $phpcsFile->getTokens();
		$prevPtr = $phpcsFile->findPrevious(T_WHITESPACE, ($stackPtr - 1), NULL, TRUE);

		// Ignore if class property name (i.e. $foo->true)
		if ($tokens[$prevPtr]['code'] === T_OBJECT_OPERATOR) {
			return;
		}

		// Ignore if class name (i.e. class True extends False implements Null)
		// or namespace name or alias name
		if ($tokens[$prevPtr]['code'] === T_CLASS ||
			$tokens[$prevPtr]['code'] === T_EXTENDS ||
			$tokens[$prevPtr]['code'] === T_IMPLEMENTS ||
			$tokens[$prevPtr]['code'] === T_NAMESPACE ||
			$tokens[$prevPtr]['code'] === T_AS) {

			return;
		}

		// Ignore if class or namespace (i.e. \Null\False\True)
		if ($tokens[($stackPtr - 1)]['code'] === T_NS_SEPARATOR) {
			return;
		}

		$keyword = $tokens[$stackPtr]['content'];
		if (strtoupper($keyword) !== $keyword) {
			$error = 'TRUE, FALSE and NULL must be uppercase; expected "%s" but found "%s"';
			$data  = array(
				strtoupper($keyword),
				$keyword,
			);
			$phpcsFile->addError($error, $stackPtr, 'Found', $data);
		}
	}
}
