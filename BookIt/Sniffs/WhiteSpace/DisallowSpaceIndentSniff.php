<?php

namespace BookIt\Sniffs\WhiteSpace;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class DisallowSpaceIndentSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_WHITESPACE);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		// Make sure the whitespace is used for indentation
		$line = $tokens[$stackPtr]['line'];
		if ($stackPtr > 0 && $tokens[$stackPtr - 1]['line'] === $line) {
			return;
		}

		if (strpos($tokens[$stackPtr]['content'], ' ') !== FALSE) {
			$error = 'Tabs must be used to indent lines; spaces are not allowed';
			$phpcsFile->addError($error, $stackPtr, 'SpacesUsedForIndent');
		}
	}
}
