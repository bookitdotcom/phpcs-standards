<?php

namespace BookIt\Sniffs\NamingConventions;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class InterfaceNameSniff implements  PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_INTERFACE);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		$class     = $phpcsFile->findNext(T_STRING, $stackPtr);
		$className = trim($tokens[$class]['content']);

		// Interface names must begin with I
		if ($className[0] != 'I') {
			$error = 'Interface name "%s" is must begin with upper case "I"; expected "%s" but found "%s"';
			$data = array(
				$className,
				'I' . $className,
				$className,
			);
			$phpcsFile->addError($error, $stackPtr, 'InterfaceBeginWithoutI', $data);
		}
	}
}
