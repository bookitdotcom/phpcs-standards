<?php

namespace BookIt\Sniffs\NamingConventions;

use PHP_CodeSniffer;
use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Standards_AbstractScopeSniff;

class FunctionNameSniff extends PHP_CodeSniffer_Standards_AbstractScopeSniff {

	public function __construct() {
		parent::__construct(array(T_CLASS, T_INTERFACE, T_TRAIT), array(T_FUNCTION), TRUE);
	}

	/**
	 * Processes a token that is found within the scope that this test is
	 * listening to.
	 *
	 * @param PHP_CodeSniffer_File $phpcsFile The file where this token was found.
	 * @param int $stackPtr  The position in the stack where this
	 *                                        token was found.
	 * @param int $currScope The position in the tokens array that
	 *                                        opened the scope that this test is
	 *                                        listening for.
	 * @return void
	 */
	protected function processTokenWithinScope(PHP_CodeSniffer_File $phpcsFile, $stackPtr, $currScope) {
		$methodName = $phpcsFile->getDeclarationName($stackPtr);
		$className  = $phpcsFile->getDeclarationName($currScope);

		// Ignore closures
		if ($methodName === NULL) {
			return;
		}

		// Magic methods
		if (preg_match('/^__/', $methodName)) {
			$magicPart = strtolower(substr($methodName, 2));
			switch ($magicPart) {
				case 'construct':
				case 'destruct':
				case 'call':
				case 'callstatic':
				case 'get':
				case 'set':
				case 'isset':
				case 'unset':
				case 'sleep':
				case 'wakeup':
				case 'tostring':
				case 'set_state':
				case 'clone':
				case 'invoke':
				case 'soapcall':
				case 'getlastrequest':
				case 'getlastresponse':
				case 'getlastrequestheaders':
				case 'getlastresponseheaders':
				case 'getfunctions':
				case 'gettypes':
				case 'dorequest':
				case 'setcookie':
				case 'setlocation':
				case 'setsoapheaders':
					break;

				default:
					$error = 'Method name "%s" is invalid; only PHP magic methods should begin with a double underscore';
					$data = array(
						$className . '::' . $methodName,
					);
					$phpcsFile->addError($error, $stackPtr, 'MethodDoubleUnderscore', $data);
			}
		} else {
			// Ensure no underscores
			if (strpos($methodName, '_')) {
				$error = 'Method names must not contain underscores; expected "%s" but found "%s"';
				$data = array(
					$className . '::' . str_replace('_', '', $methodName),
					$className . '::' . $methodName,
				);
				$phpcsFile->addError($error, $stackPtr, 'MethodUnderscores', $data);
			}

			// Method names must be lower-camel case
			if (!PHP_CodeSniffer::isCamelCaps($methodName, FALSE, TRUE, FALSE)) {
				$error = 'Method name "%s" is not in lower camel case';
				$data = array(
					$className . '::' . $methodName,
				);
				$phpcsFile->addError($error, $stackPtr, 'MethodNotLowerCamelCase', $data);
			}
		}
	}

	/**
	 * Processes the tokens outside the scope.
	 *
	 * @param PHP_CodeSniffer_File $phpcsFile The file being processed.
	 * @param int                  $stackPtr  The position where this token was
	 *                                        found.
	 * @return void
	 */
	protected function processTokenOutsideScope(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$functionName = $phpcsFile->getDeclarationName($stackPtr);

		// Ignore closures
		if ($functionName === NULL) {
			return;
		}

		// Magic functions
		if (preg_match('/^__/', $functionName)) {
			$magicPart = strtolower(substr($functionName, 2));
			switch ($functionName) {
				case 'autoload':
					break;

				default:
					$error = 'Function name "%s" is invalid; only PHP magic functions should begin with a double underscore';
					$data = array(
						$functionName,
					);
					$phpcsFile->addError($error, $stackPtr, 'FunctionDoubleUnderscore', $data);
			}
		} else {
			// Ensure no underscores
			if (strpos($functionName, '_')) {
				$error = 'Function names must not contain underscores; expected "%s" but found "%s"';
				$data = array(
					str_replace('_', '', $functionName),
					$functionName,
				);
				$phpcsFile->addError($error, $stackPtr, 'FunctionUnderscores', $data);
			}
		}

		// Function names must be lower-camel case
		if (!PHP_CodeSniffer::isCamelCaps($functionName, FALSE, TRUE, FALSE)) {
			$error = 'Function name "%s" is not in lower camel case';
			$data = array(
				$functionName,
			);
			$phpcsFile->addError($error, $stackPtr, 'FunctionNotLowerCamelCase', $data);
		}
	}

}
