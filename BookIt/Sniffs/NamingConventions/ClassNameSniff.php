<?php

namespace BookIt\Sniffs\NamingConventions;

use PHP_CodeSniffer;
use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class ClassNameSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_CLASS, T_INTERFACE);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		$class     = $phpcsFile->findNext(T_STRING, $stackPtr);
		$className = trim($tokens[$class]['content']);

		// Class names must be upper-camel case
		if (!PHP_CodeSniffer::isCamelCaps($className, TRUE, TRUE, FALSE)) {
			$error = 'Class name "%s" is not in upper camel case';
			$data = array(
				$className,
			);
			$phpcsFile->addError($error, $stackPtr, 'ClassNotUpperCamelCase', $data);
		}
	}
}
