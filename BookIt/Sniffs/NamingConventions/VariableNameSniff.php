<?php

namespace BookIt\Sniffs\NamingConventions;

use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Standards_AbstractVariableSniff;

class VariableNameSniff extends PHP_CodeSniffer_Standards_AbstractVariableSniff {

	/**
	 * Called to process class member vars.
	 *
	 * @param PHP_CodeSniffer_File $phpcsFile The PHP_CodeSniffer file where this
	 * @param int $stackPtr  The position where the token was found.
	 * @return void
	 */
	protected function processMemberVar(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();
		$props = $phpcsFile->getMemberProperties($stackPtr);

		$varName = ltrim($tokens[$stackPtr]['content'], '$');

		// Make sure a scope is specified
		if (!$props['scope_specified']) {
			$error = 'No variable scope specified for variable "$%s"';
			$data = array(
				$varName,
			);
			$phpcsFile->addError($error, $stackPtr, 'NoVariableScope', $data);
		}

		$scope = $props['scope'];

		// Private/protected vars must start with an underscore
		if (($scope == 'protected' || $scope == 'private') && $varName[0] != '_') {
			$error = 'Protected/private variable names must start with an underscore; expected "$_%s" but found "$%s"';
			$data = array(
				$varName,
				$varName,
			);
			$phpcsFile->addError($error, $stackPtr, 'PrivateUnderscore', $data);
		}

		// Public vars must not start with an underscore
		if (($scope == 'public' || !$props['scope_specified']) && $varName[0] == '_') {
			$error = 'Public variable names must not start with an underscore; expected "$%s" but found "$%s"';
			$data = array(
				substr($varName, 1),
				$varName,
			);
			$phpcsFile->addError($error, $stackPtr, 'PublicUnderscore', $data);
		}

		// Ensure no underscores in variable name after initial underscore
		if (strrpos($varName, '_') > 0) {
			$error = 'Variable names should not contain underscores; expected "%s" but found "%s"';
			$data = array(
				$varName[0] . str_replace('_', '', substr($varName, 1)),
				$varName,
			);
			$phpcsFile->addError($error, $stackPtr, 'ClassVariableUnderscore', $data);
		}
	}

	/**
	 * Called to process normal member vars.
	 *
	 * @param PHP_CodeSniffer_File $phpcsFile The PHP_CodeSniffer file where this
	 * @param int $stackPtr  The position where the token was found.
	 * @return void
	 */
	protected function processVariable(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		// Ensure the variable beings with a lower case letter
		$varName = ltrim($tokens[$stackPtr]['content'], '$');
		if ($varName[0] != strtolower($varName[0])) {
			$error = 'Variable names must begin with a lower case character; expected "%s" but found "%s"';
			$data = array(
				strtolower($varName[0]) . substr($varName, 1),
				$varName,
			);
			$phpcsFile->addError($error, $stackPtr, 'UpperCaseVariable', $data);
		}

		// Ensure no underscores in variable name
		if (strpos($varName, '_')) {
			$error = 'Variable names should not contain underscores; expected "%s" but found "%s"';
			$data = array(
				str_replace('_', '', $varName),
				$varName,
			);
			$phpcsFile->addError($error, $stackPtr, 'VariableUnderscore', $data);
		}
	}

	/**
	 * Called to process variables found in double quoted strings or heredocs.
	 *
	 * Note that there may be more than one variable in the string, which will
	 * result only in one call for the string or one call per line for heredocs.
	 *
	 * @param PHP_CodeSniffer_File $phpcsFile The PHP_CodeSniffer file where this
	 * @param int $stackPtr  The position where the double quoted
	 *                                        string was found.
	 * @return void
	 */
	protected function processVariableInString(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		// Not implemented
	}
}
