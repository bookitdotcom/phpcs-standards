<?php

namespace BookIt\Sniffs\Commenting;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class FunctionDocblockSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_FUNCTION);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		return;

		$tokens = $phpcsFile->getTokens();
		$fnName = $phpcsFile->getDeclarationName($stackPtr);

		$class = $phpcsFile->findPrevious(array(T_CLASS, T_INTERFACE), $stackPtr - 1);
		if ($this->methodInClass($phpcsFile, $class, $stackPtr)) {
			$className = $phpcsFile->getDeclarationName($class);
			$fqcn = '\\' . $className;

			$nsKeyword = $phpcsFile->findPrevious(T_NAMESPACE, $class - 1);
			if ($nsKeyword !== FALSE) {
				$namespace = $phpcsFile->findNext(T_STRING, $nsKeyword, $tokens[$nsKeyword]['scope_opener']);

				if ($namespace !== FALSE) {
					$fqcn = '\\' . $tokens[$namespace]['content'] . $fqcn;
				}
			}

			$classReflection = new \ReflectionClass($fqcn);
			$reflection = $classReflection->getMethod($fnName);

			$error = 'Class method %s::%s() ';
			$data = array(
				$fqcn,
				$fnName,
			);
		} else {
			$reflection = new \ReflectionFunction($fnName);

			$error = 'Function %s() ';
			$data = array(
				$fnName,
			);
		}

		$docComment = $reflection->getDocComment();
		if ($docComment === FALSE) {
			$error .= 'missing doc block comment';
			$phpcsFile->addError($error, $stackPtr, 'MissingFunctionDocBlock', $data);
			return;
		}

		// Check @params
		foreach ($reflection->getParameters() as $param) {
			if ($param->isArray()) {
				$type = 'array\s+';
				$dataType = 'array';
			} elseif ($param->isCallable()) {
				$type = 'callable\s+';
				$dataType = 'callable';
			} elseif ($param->getClass() instanceof ReflectionClass) {
				$type = '([A-Z0-9_\\\\]+)\s+';
				$dataType = $param->getClass()->name;
			} else {
				$type = '([A-Z0-9_\\\\]*)\s*';
			}

			$pattern = '/@param\s+' . $type . '\$' . $param->getName() . '/i';
			$typeless = '/@param\s+([A-Z0-9_\\\\]*)\s*\$' . $param->getName() . '/i';

			if (!preg_match($pattern, $docComment, $matches)) {
				if (!preg_match($typeless, $docComment, $typeMatches)) {
					$tagError = $error . 'missing @param tag for argument $%s';

					$tagData = array_slice($data, 0);
					$tagData[] = $param->getName();

					$phpcsFile->addError($tagError, $stackPtr, 'MissingParamTag', $tagData);
				} else {
					if (isset($typeMatches[1]) && strlen($typeMatches[1]) > 0) {
						$tagError = $error . 'incorrect data type for @param tag for argument $%s; expected %s, got %s';
					} else {
						$tagError = $error . 'missing data type for @param tag for argument $%s; expected %s';
					}

					$tagData = array_slice($data, 0);
					$tagData[] = $param->getName();
					$tagData[] = $dataType;
					$tagData[] = strval($typeMatches[1]);

					$phpcsFile->addWarning($tagError, $stackPtr, 'MissingParamTag', $tagData);
				}
			}
		}
	}

	protected function methodInClass(PHP_CodeSniffer_File $phpcsFile, $class, $method, array $foo = array()) {
		if ($class === FALSE) {
			return FALSE;
		}

		$tokens = $phpcsFile->getTokens();

		if ($method < $tokens[$class]['scope_opener']) {
			return FALSE;
		}

		if ($method > $tokens[$class]['scope_closer']) {
			return FALSE;
		}

		return TRUE;
	}

}
