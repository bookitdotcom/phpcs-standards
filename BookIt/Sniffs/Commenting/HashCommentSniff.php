<?php

namespace BookIt\Sniffs\Commenting;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class HashCommentSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(
			T_COMMENT,
		);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		if ($tokens[$stackPtr]['content'][0] === '#') {
			$error = 'Comments must use double slashes or slash/star syntax; found #';
			$phpcsFile->addError($error, $stackPtr, 'Found');
		}
	}
}
