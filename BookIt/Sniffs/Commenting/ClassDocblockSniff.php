<?php

namespace BookIt\Sniffs\Commenting;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class ClassDocblockSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_CLASS, T_INTERFACE);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		return;

		$tokens = $phpcsFile->getTokens();

		$className = $phpcsFile->getDeclarationName($stackPtr);
		$fqcn = '\\' . $className;

		$nsKeyword = $phpcsFile->findPrevious(T_NAMESPACE, $stackPtr -1);
		if ($nsKeyword !== FALSE) {
			$namespace = $phpcsFile->findNext(T_STRING, $nsKeyword, $tokens[$nsKeyword]['scope_opener']);

			if ($namespace !== FALSE) {
				$fqcn = '\\' . $tokens[$namespace]['content'] . $fqcn;
			}
		}

		$reflection = new \ReflectionClass($fqcn);

		if ($reflection->getDocComment() === FALSE) {
			$error = '%s %s missing doc block comment';
			$data = array(
				ucfirst($tokens[$stackPtr]['content']),
				$fqcn,
			);
			$phpcsFile->addError($error, $stackPtr, 'MissingClassDocBlock', $data);
		}
	}
}