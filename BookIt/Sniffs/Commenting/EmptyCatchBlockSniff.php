<?php

namespace BookIt\Sniffs\Commenting;

use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Sniff;

class EmptyCatchBlockSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_CATCH);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		if (!isset($tokens[$stackPtr]['scope_opener']) ||
			!isset($tokens[$stackPtr]['scope_opener'])) {

			return;
		}

		$open = $tokens[$stackPtr]['scope_opener'];
		$close = $tokens[$stackPtr]['scope_closer'];

		if ($phpcsFile->findNext(T_WHITESPACE, $open + 1, $close + 1, TRUE) === $close) {
			$phpcsFile->addWarning(
				'Empty catch blocks should include a comment indicating why the exception can be silenced',
				$stackPtr,
				'ExplanationRequired'
			);
		}
	}
}
