<?php

namespace BookIt\Sniffs\Formatting;

use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Sniff;


class NestedTernarySniff implements PHP_CodeSniffer_Sniff {

	/**
	 * Registers the tokens that this sniff wants to listen for.
	 *
	 * An example return value for a sniff that wants to listen for whitespace
	 * and any comments would be:
	 *
	 * <code>
	 *    return array(
	 *            T_WHITESPACE,
	 *            T_DOC_COMMENT,
	 *            T_COMMENT,
	 *           );
	 * </code>
	 *
	 * @return array(int)
	 * @see    Tokens.php
	 */
	public function register() {
		return array(T_INLINE_THEN);
	}

	/**
	 * Called when one of the token types that this sniff is listening for
	 * is found.
	 *
	 * The stackPtr variable indicates where in the stack the token was found.
	 * A sniff can acquire information this token, along with all the other
	 * tokens within the stack by first acquiring the token stack:
	 *
	 * <code>
	 *    $tokens = $phpcsFile->getTokens();
	 *    echo 'Encountered a '.$tokens[$stackPtr]['type'].' token';
	 *    echo 'token information: ';
	 *    print_r($tokens[$stackPtr]);
	 * </code>
	 *
	 * If the sniff discovers an anomaly in the code, they can raise an error
	 * by calling addError() on the PHP_CodeSniffer_File object, specifying an error
	 * message and the position of the offending token:
	 *
	 * <code>
	 *    $phpcsFile->addError('Encountered an error', $stackPtr);
	 * </code>
	 *
	 * @param PHP_CodeSniffer_File $phpcsFile The PHP_CodeSniffer file where the
	 *                                        token was found.
	 * @param int $stackPtr The position in the PHP_CodeSniffer
	 *                                        file's token stack where the token
	 *                                        was found.
	 *
	 * @return void
	 */
	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {

		$tokens = $phpcsFile->getTokens();

		$end = NULL;

		if (isset($tokens[$stackPtr]['nested_parenthesis'])) {
			// search until the end of parenthesis or a comma (which ever comes first)
			$closePtr = current($tokens[$stackPtr]['nested_parenthesis']); // closing parenthesis
			$comma    = $phpcsFile->findNext(T_COMMA, $stackPtr + 1, $closePtr); // a comma before the closing parenthesis
			$end      = $comma ? : $closePtr;
		}

		if ($ptr = $phpcsFile->findNext(T_INLINE_THEN, $stackPtr + 1, $end, FALSE, NULL, TRUE)) {
			$error = 'Nested ternary statement are not permitted';
			$phpcsFile->addError($error, $ptr, 'NestedTernary');
		}
	}
}
