<?php

namespace BookIt\Sniffs\Formatting;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Tokens;

class OneTBSSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(
			T_CLASS,
			T_INTERFACE,
			T_FUNCTION,
			T_CLOSURE,
			T_NAMESPACE,
			T_IF,
			T_ELSEIF,
			T_ELSE,
			T_SWITCH,
			T_DO,
			T_FOR,
			T_FOREACH,
			T_TRY,
			T_WHILE,
			T_CATCH,
			T_DECLARE,
		);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$this->processOpeningBrace($phpcsFile, $stackPtr);
		$this->processClosingBrace($phpcsFile, $stackPtr);
	}

	public function processOpeningBrace(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		if ($tokens[$stackPtr]['code'] === T_DECLARE) {
			$braces = $this->findBraces($phpcsFile, $stackPtr);
			if ($braces['open']) {
				$tokens[$stackPtr]['scope_opener'] = $braces['open'];
			} else {
				return;
			}
			if ($braces['close']) {
				$tokens[$stackPtr]['scope_closer'] = $braces['close'];
			} else {
				return;
			}
		}

		if (!$this->checkScopeOpener($phpcsFile, $stackPtr)) {
			return;
		}

		$brace = $tokens[$stackPtr]['scope_opener'];
		$braceLine = $tokens[$brace]['line'];

		$compareLine = NULL;
		$error = FALSE;
		$data = array();

		switch ($tokens[$stackPtr]['code']) {
			case T_CLASS:
			case T_INTERFACE:
				$implements = $phpcsFile->findNext(T_IMPLEMENTS, $stackPtr, $tokens[$stackPtr]['scope_opener']);
				if ($implements !== FALSE) {
					// Brace must be on the same line as the last interface name
					$lastImplements = $phpcsFile->findPrevious(T_STRING, $tokens[$stackPtr]['scope_opener'], $stackPtr);

					if ($lastImplements === FALSE) {
						$error = 'Possible parse error; no interface found between T_IMPLEMENTS and opening brace';
						$phpcsFile->addError($error, $stackPtr, 'NoInterfaceFound');
						return;
					}

					$compareLine = $tokens[$lastImplements]['line'];
					$error = 'Opening brace expected on the same line as the last interface name';
					$data[] = $tokens[$stackPtr]['content'];

				} else {
					// Brace must be on the same line as class/interface declaration
					$compareLine = $tokens[$stackPtr]['line'];

					$error  = 'Opening brace expected on the same line as %s declaration';
					$data[] = $tokens[$stackPtr]['content'];
				}
				break;

			case T_FUNCTION:
			case T_IF:
			case T_ELSEIF:
			case T_FOR:
			case T_FOREACH:
			case T_WHILE:
				// Brace must be on the same line as the closing parenthesis
				if (!isset($tokens[$stackPtr]['parenthesis_closer'])) {
					$error = 'Possible parse error; no closing parenthesis found for structure %s';
					$data[] = $tokens[$stackPtr]['content'];
					$phpcsFile->addError($error, $stackPtr, 'NoClosingParenFound', $data);
					return;
				}

				$paren = $tokens[$stackPtr]['parenthesis_closer'];
				$compareLine = $tokens[$paren]['line'];

				$error = 'Opening brace expected on the same line as closing parenthesis for structure %s';
				$data[] = $tokens[$stackPtr]['content'];
				break;

			case T_CLOSURE:
				$use = $phpcsFile->findNext(T_USE, $stackPtr, $tokens[$stackPtr]['scope_opener']);
				if ($use !== FALSE) {
					// Brace must be on the same line as the closing parenthesis of the use
					if (!isset($tokens[$stackPtr]['parenthesis_closer'])) {
						$error = 'Possible parse error; no closing parenthesis found for structure %s';
						$data[] = $tokens[$stackPtr]['content'];
						$phpcsFile->addError($error, $stackPtr, 'NoClosingParenFound', $data);
						return;
					}

					$openParen = $phpcsFile->findNext(T_OPEN_PARENTHESIS, $use, $tokens[$stackPtr]['scope_opener']);
					if ($openParen !== FALSE) {
						$closeParen = $tokens[$openParen]['parenthesis_closer'];
						$compareLine = $tokens[$closeParen]['line'];
					}

				} else {
					// Brace msut be on the same line as the closing parenthesis
					if (!isset($tokens[$stackPtr]['parenthesis_closer'])) {
						$error = 'Possible parse error; no closing parenthesis found for structure %s';
						$data[] = $tokens[$stackPtr]['content'];
						$phpcsFile->addError($error, $stackPtr, 'NoClosingParenFound', $data);
						return;
					}

					$paren = $tokens[$stackPtr]['parenthesis_closer'];
					$compareLine = $tokens[$paren]['line'];
				}

				$error = 'Opening brace expected on the same line as closing parenthesis for structure %s';
				$data[] = $tokens[$stackPtr]['content'];
				break;

			case T_NAMESPACE:
			case T_ELSE:
			case T_SWITCH:
			case T_DO:
			case T_TRY:
			case T_CATCH:
			case T_DECLARE:
				// Brace must be on the same line as declaration
				$compareLine = $tokens[$stackPtr]['line'];

				$error  = 'Opening brace expected on the same line as structure %s';
				$data[] = $tokens[$stackPtr]['content'];
				break;
		}

		if ($compareLine !== NULL && $compareLine !== $braceLine) {
			$phpcsFile->addError($error, $brace, 'OpenBraceViolation', $data);
		}
	}

	public function processClosingBrace(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		if ($tokens[$stackPtr]['code'] === T_DECLARE) {
			$braces = $this->findBraces($phpcsFile, $stackPtr);
			if ($braces['open']) {
				$tokens[$stackPtr]['scope_opener'] = $braces['open'];
			} else {
				return;
			}
			if ($braces['close']) {
				$tokens[$stackPtr]['scope_closer'] = $braces['close'];
			} else {
				return;
			}
		}

		if (!isset($tokens[$stackPtr]['scope_closer'])) {
			return;
		}

		$brace = $tokens[$stackPtr]['scope_closer'];
		$braceLine = $tokens[$brace]['line'];

		$compareLine = NULL;
		$nextStruct = FALSE;

		switch ($tokens[$stackPtr]['code']) {
			case T_CLOSURE:
				// Closures can be used in line
				break;

			case T_IF:
			case T_ELSEIF:
			case T_ELSE:
				$lookAhead = $phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, $brace + 1, NULL, TRUE);
				if ($tokens[$lookAhead]['code'] === T_ELSE || $tokens[$lookAhead]['code'] === T_ELSEIF) {
					$nextStruct = $lookAhead;
				}

			case T_DO:
				$lookAhead = $phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, $brace + 1, NULL, TRUE);
				if ($tokens[$lookAhead]['code'] === T_WHILE) {
					$nextStruct = $lookAhead;
				}

			case T_TRY:
			case T_CATCH:
				$lookAhead = $phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, $brace + 1, NULL, TRUE);
				if ($tokens[$lookAhead]['code'] === T_CATCH) {
					$nextStruct = $lookAhead;
				}

			default:
				// Closing brace must be by itself on the line
				$cPtr = $brace + 1;
				while (isset($tokens[$cPtr]) && $tokens[$cPtr]['content'] !== $phpcsFile->eolChar) {
					if ($cPtr === $nextStruct) {
						// Ignore the "next structure" for if/elseif/else, do/while, and try/catch
						break;
					}
					if ($tokens[$cPtr]['code'] === T_WHITESPACE) {
						// Whitespace is ignored
						$cPtr++;
						continue;
					}
					if ($tokens[$cPtr]['code'] === T_COMMENT) {
						// Comments are ignored
						$cPtr++;
						continue;
					}
					if ($tokens[$stackPtr]['code'] === T_CLOSURE && $tokens[$cPtr]['code'] === T_SEMICOLON) {
						// Ignore semicolons after closures
						$cPtr++;
						continue;
					}

					// If we got here, we found illegal characters
					$error = 'Closing brace must be alone on line; found content after brace: %s';
					$data = array(
						$tokens[$cPtr]['content'],
					);
					$phpcsFile->addError($error, $cPtr, 'ContentAfterClosingBrace', $data);
					break;
				}

				$cPtr = $brace - 1;
				while ($tokens[$cPtr]['line'] === $tokens[$brace]['line']) {
					if ($tokens[$cPtr]['code'] === T_WHITESPACE) {
						// Whitespace is ignored
						$cPtr--;
						continue;
					}

					// If we got here, we found illegal characters
					$error = 'Closing brace must be alone on line; found content before brace: %s';
					$data = array(
						$tokens[$cPtr]['content'],
					);
					$phpcsFile->addError($error, $cPtr, 'ContentBeforeClosingBrace', $data);
					break;
				}
		}

		// Closing brace must be on the same line as the next declaration
		if ($nextStruct) {
			$compareLine = $tokens[$nextStruct]['line'];

			if ($compareLine !== $braceLine) {
				$error = 'Closing brace of structure %s must be on the same line as subsequent keyword %s';
				$data = array(
					$tokens[$stackPtr]['content'],
					$tokens[$nextStruct]['content'],
				);
				$phpcsFile->addError($error, $brace, 'CloseBraceViolation', $data);
			}
		}
	}

	/**
	 * Checks the current token for a scope opener
	 *
	 * @param PHP_CodeSniffer_File $phpcsFile
	 * @param int $stackPtr
	 * @return boolean TRUE if checks can continue, FALSE if they cannot
	 */
	protected function checkScopeOpener(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();
		$ctoken = $tokens[$stackPtr];

		if (!isset($ctoken['scope_opener'])) {

			// Namespaces do not need scope openers
			if ($ctoken['code'] === T_NAMESPACE) {
				return FALSE;
			}

			// Functions do not need scope openers in an interface
			if ($ctoken['code'] === T_FUNCTION) {
				return FALSE;
			}

			// Declare doesn't need a scope opener, but we've already taken care of that by now
			if ($ctoken['code'] === T_DECLARE) {
				return TRUE;
			}

			// In a split else-if, the else does not need a scope opener
			if ($ctoken['code'] === T_ELSE) {
				// Fast forward over whitespace
				$nextPtr = $phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, $stackPtr + 1, NULL, TRUE);

				// Split else-if
				if ($tokens[$nextPtr]['code'] === T_IF) {
					return FALSE;
				}
			}

			// In a do-while loop, the while does not need a scope opener
			if ($ctoken['code'] === T_WHILE) {
				// Rewind over whitespace
				$prevPtr = $phpcsFile->findPrevious(PHP_CodeSniffer_Tokens::$emptyTokens, $stackPtr - 1, NULL, TRUE);

				if ($tokens[$prevPtr]['code'] === T_CLOSE_CURLY_BRACKET) {
					$condition = $tokens[$prevPtr]['scope_condition'];
					if ($tokens[$condition]['code'] === T_DO) {
						return FALSE;
					}
				}
			}

			// We expect a scope opener and yet we don't have one
			$error = 'Structure %s missing opening brace';
			$data = array(
				$tokens[$stackPtr]['content'],
			);
			$phpcsFile->addError($error, $stackPtr, 'MissingOpeningBrace', $data);

			return FALSE;
		}

		return TRUE;
	}

	protected function findBraces(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		$braces = array(
			'open' => FALSE,
			'close' => FALSE,
		);

		// Fast forward over whitespace to open parenthesis
		$nextPtr = $phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, $stackPtr + 1, NULL, TRUE);

		if ($tokens[$nextPtr]['code'] === T_OPEN_PARENTHESIS) {
			$openParen = $nextPtr;
			if (isset($tokens[$openParen]) && isset($tokens[$openParen]['parenthesis_closer'])) {
				// Fast forward over whitespace to brace
				$nextPtr = $phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, $tokens[$openParen]['parenthesis_closer'] + 1, NULL, TRUE);

				if ($tokens[$nextPtr]['code'] === T_OPEN_CURLY_BRACKET) {
					$braces['open'] = $nextPtr;
					if (isset($tokens[$nextPtr]['bracket_closer'])) {
						$braces['close'] = $tokens[$nextPtr]['bracket_closer'];
					}
				}
			}
		}

		return $braces;
	}
}
