<?php

namespace BookIt\Sniffs\Formatting;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class CastPaddingSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(
			T_ARRAY_CAST,
			T_BOOL_CAST,
			T_DOUBLE_CAST,
			T_INT_CAST,
			T_OBJECT_CAST,
			T_STRING_CAST,
			T_UNSET_CAST,
		);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		// Check for whitespace after a cast
		if (isset($tokens[$stackPtr + 1]) && $tokens[$stackPtr + 1]['code'] !== T_WHITESPACE) {
			$error = 'Cast %s must be followed by a space; found %s';
			$data = array(
				$tokens[$stackPtr]['content'],
				$tokens[$stackPtr + 1]['content'],
			);
			$phpcsFile->addError($error, $stackPtr, 'CastPaddingViolation', $data);
		}
	}
}

