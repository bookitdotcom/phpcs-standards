<?php

namespace BookIt\Sniffs\Formatting;

use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_Tokens;

class PhpInPhtmlSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_OPEN_TAG, T_CLOSE_TAG);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		// Skip if close tag for short open echo
		if ($tokens[$stackPtr]['code'] === T_CLOSE_TAG) {
			$opener = $phpcsFile->findPrevious(array(T_OPEN_TAG, T_OPEN_TAG_WITH_ECHO), $stackPtr - 1);
			if ($tokens[$opener]['code'] === T_OPEN_TAG_WITH_ECHO) {
				return;
			}
		}

		// Make sure PHP tag is on the same line
		if ($tokens[$stackPtr]['code'] === T_OPEN_TAG) {
			$sibling = $phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, $stackPtr + 1, NULL, TRUE);
		} elseif ($tokens[$stackPtr]['code'] === T_CLOSE_TAG) {
			$sibling = $phpcsFile->findPrevious(PHP_CodeSniffer_Tokens::$emptyTokens, $stackPtr - 1, NULL, TRUE);
		} else {
			// Theoretically impossible
			return;
		}

		if ($tokens[$sibling]['line'] === $tokens[$stackPtr]['line']) {
			$phpcsFile->addError(
				'PHP tags interrupting a PHTML file must be on separate lines; expected "%s" but found "%s %s"',
				$stackPtr,
				'PhpTagsInline',
				array(
					trim($tokens[$stackPtr]['content']),
					trim($tokens[$stackPtr]['code'] === T_OPEN_TAG ? $tokens[$stackPtr]['content'] : $tokens[$sibling]['content']),
					trim($tokens[$stackPtr]['code'] === T_OPEN_TAG ? $tokens[$sibling]['content'] : $tokens[$stackPtr]['content']),
				)
			);
		}
	}
}
