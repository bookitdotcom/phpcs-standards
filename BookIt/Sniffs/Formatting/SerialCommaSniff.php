<?php

namespace BookIt\Sniffs\Formatting;

use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_Tokens;

class SerialCommaSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_ARRAY);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		if (!isset($tokens[$stackPtr]['parenthesis_opener']) ||
			!isset($tokens[$stackPtr]['parenthesis_closer'])) {

			return;
		}

		$open = $tokens[$stackPtr]['parenthesis_opener'];
		$close = $tokens[$stackPtr]['parenthesis_closer'];

		// Skip if open and close are on the same line
		if ($tokens[$open]['line'] === $tokens[$close]['line']) {
			return;
		}

		// Find the last non-empty token before the closer
		$prevPtr = $phpcsFile->findPrevious(PHP_CodeSniffer_Tokens::$emptyTokens, $close - 1, NULL, TRUE);

		if ($tokens[$prevPtr]['code'] !== T_COMMA) {
			$phpcsFile->addError(
				'Last element in a multi-line array assignment must have a comma; expected "%s," but found "%s"',
				$prevPtr,
				'MissingLastComma',
				array(
					$tokens[$prevPtr]['content'],
					$tokens[$prevPtr]['content'],
				)
			);
		}
	}
}
