<?php

namespace BookIt\Sniffs\Formatting;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Tokens;

class ParenthesisPaddingSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(
			T_FUNCTION,
			T_CLOSURE,
			T_USE,
			T_IF,
			T_ELSEIF,
			T_SWITCH,
			T_FOR,
			T_FOREACH,
			T_WHILE,
			T_CATCH,
			T_DECLARE,
		);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		// If use, find parenthesis manually
		if ($tokens[$stackPtr]['code'] === T_USE || $tokens[$stackPtr]['code'] === T_DECLARE) {
			$parens = $this->findParentheses($phpcsFile, $stackPtr);
			if ($parens['open']) {
				$tokens[$stackPtr]['parenthesis_opener'] = $parens['open'];
			}
			if ($parens['close']) {
				$tokens[$stackPtr]['parenthesis_closer'] = $parens['close'];
			}
		}

		// Check for whitespace before an opening parenthesis
		if (isset($tokens[$stackPtr]['parenthesis_opener'])) {
			$paren = $tokens[$stackPtr]['parenthesis_opener'];

			if ($tokens[$stackPtr]['code'] !== T_FUNCTION
				&& $tokens[$stackPtr]['code'] !== T_CLOSURE
				&& isset($tokens[$paren - 1])
				&& $tokens[$paren - 1]['code'] !== T_WHITESPACE) {

				$error = 'Opening parenthesis for structure %s must be preceded by a space; found %s';
				$data = array(
					$tokens[$stackPtr]['content'],
					$tokens[$paren - 1]['content'],
				);
				$phpcsFile->addError($error, $paren, 'OpeningParenPaddingViolation', $data);
			}
		}

		// Check for whitespace after a closing parenthesis
		if (isset($tokens[$stackPtr]['parenthesis_closer'])) {
			$paren = $tokens[$stackPtr]['parenthesis_closer'];

			if (isset($tokens[$paren + 1]) &&
				$tokens[$paren + 1]['code'] !== T_WHITESPACE &&
				!($tokens[$stackPtr]['code'] === T_FUNCTION && !isset($tokens[$stackPtr]['scope_opener'])) &&
				!($tokens[$stackPtr]['code'] === T_WHILE && !isset($tokens[$stackPtr]['scope_opener']))) {

				$error = 'Closing parenthesis for structure %s must be followed by a space; found %s';
				$data = array(
					$tokens[$stackPtr]['content'],
					$tokens[$paren + 1]['content'],
				);
				$phpcsFile->addError($error, $paren, 'ClosingParenPaddingViolation', $data);
			}
		}
	}

	protected function findParentheses(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		$parens = array(
			'open' => FALSE,
			'close' => FALSE,
		);

		// Fast forward over whitespace
		$nextPtr = $phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, $stackPtr + 1, NULL, TRUE);

		if ($tokens[$nextPtr]['code'] === T_OPEN_PARENTHESIS) {
			$parens['open'] = $nextPtr;
			if ($parens['open'] !== FALSE) {
				$parens['close'] = $tokens[$parens['open']]['parenthesis_closer'];
			}
		}

		return $parens;
	}

}
