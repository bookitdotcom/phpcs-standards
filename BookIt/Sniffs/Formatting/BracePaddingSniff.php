<?php

namespace BookIt\Sniffs\Formatting;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Tokens;

class BracePaddingSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(
			T_CLASS,
			T_INTERFACE,
			T_FUNCTION,
			T_CLOSURE,
			T_NAMESPACE,
			T_IF,
			T_ELSEIF,
			T_ELSE,
			T_SWITCH,
			T_DO,
			T_FOR,
			T_FOREACH,
			T_TRY,
			T_WHILE,
			T_CATCH,
			T_DECLARE,
		);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		if ($tokens[$stackPtr]['code'] === T_DECLARE) {
			$braces = $this->findBraces($phpcsFile, $stackPtr);
			if ($braces['open']) {
				$tokens[$stackPtr]['scope_opener'] = $braces['open'];
			}
			if ($braces['close']) {
				$tokens[$stackPtr]['scope_closer'] = $braces['close'];
			}
		}

		// Check for whitespace before opening brace
		if (isset($tokens[$stackPtr]['scope_opener'])) {
			$brace = $tokens[$stackPtr]['scope_opener'];

			if (isset($tokens[$brace - 1]) && $tokens[$brace - 1]['code'] !== T_WHITESPACE) {
				$error = 'Opening brace for structure %s must be preceded by a space; found %s';
				$data = array(
					$tokens[$stackPtr]['content'],
					$tokens[$brace - 1]['content'],
				);
				$phpcsFile->addError($error, $brace, 'OpeningBracePaddingViolation', $data);
			}
		}

		// Check for whitespace after closing brace
		if (isset($tokens[$stackPtr]['scope_closer'])) {
			$brace = $tokens[$stackPtr]['scope_closer'];

			if ($tokens[$stackPtr]['code'] !== T_CLOSURE) {
				if (isset($tokens[$brace + 1]) && $tokens[$brace + 1]['code'] !== T_WHITESPACE) {
					$error = 'Closing brace for structure %s must be followed by a space; found %s';
					$data = array(
						$tokens[$stackPtr]['content'],
						$tokens[$brace + 1]['content'],
					);
					$phpcsFile->addError($error, $brace, 'ClosingBracePaddingViolation', $data);
				}
			}
		}
	}

	protected function findBraces(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		$braces = array(
			'open' => FALSE,
			'close' => FALSE,
		);

		// Fast forward over whitespace to open parenthesis
		$nextPtr = $phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, $stackPtr + 1, NULL, TRUE);

		if ($tokens[$nextPtr]['code'] === T_OPEN_PARENTHESIS) {
			$openParen = $nextPtr;
			if (isset($tokens[$openParen]) && isset($tokens[$openParen]['parenthesis_closer'])) {
				// Fast forward over whitespace to brace
				$nextPtr = $phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, $tokens[$openParen]['parenthesis_closer'] + 1, NULL, TRUE);

				if ($tokens[$nextPtr]['code'] === T_OPEN_CURLY_BRACKET) {
					$braces['open'] = $nextPtr;
					if (isset($tokens[$nextPtr]['bracket_closer'])) {
						$braces['close'] = $tokens[$nextPtr]['bracket_closer'];
					}
				}
			}
		}

		return $braces;
	}
}
