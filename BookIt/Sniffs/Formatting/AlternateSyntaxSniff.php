<?php

namespace BookIt\Sniffs\Formatting;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class AlternateSyntaxSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(
			T_IF,
			T_ELSEIF,
			T_ELSE,
			T_SWITCH,
			T_WHILE,
			T_FOR,
			T_FOREACH,
		);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();
		$opener = FALSE;

		if (isset($tokens[$stackPtr]['scope_opener'])) {
			$opener = $tokens[$stackPtr]['scope_opener'];
		} else {
			$start = isset($tokens[$stackPtr]['parenthesis_closer']) ? $tokens[$stackPtr]['parenthesis_closer'] : $stackPtr;
			$nextPtr = $phpcsFile->findNext(\PHP_CodeSniffer_Tokens::$emptyTokens, $start + 1, NULL, TRUE);

			switch ($tokens[$nextPtr]['code']) {
				case T_OPEN_CURLY_BRACKET:
				case T_COLON:
					$opener = $nextPtr;
					break;
			}
		}

		if ($opener) {
			if ($tokens[$opener]['code'] === T_COLON) {
				$error = 'Structure %s must not use alternate colon/end syntax';
				$data = array(
					$tokens[$stackPtr]['content'],
				);
				$phpcsFile->addError($error, $stackPtr, 'AlternateSyntax', $data);
			}
		}
	}

}
