<?php

namespace BookIt\Sniffs\Performance;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class ArrayFunctionsSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_STRING);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		$extraMessage = '';

		switch ($tokens[$stackPtr]['content']) {
			case 'in_array':
			case 'array_diff':
				$extraMessage = ' Instead, try to reformat your data so that a more efficient method can be used.';
				break;
			case 'array_filter':
			case 'array_key_exists':
			case 'array_merge':
			case 'array_map':
			case 'array_unique':
			case 'end':
				break;
			default:
				return;
		}

		$phpcsFile->addWarning(
			'Array function "%s()" should not be used unless absolutely necessary and should be refactored out of existing code if at all possible.%s',
			$stackPtr,
			'InefficientFunction',
			array(
				$tokens[$stackPtr]['content'],
				$extraMessage,
			)
		);
	}
}
