<?php

namespace BookIt\Sniffs\Globals;

use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Sniff;

class RegistrySingletonSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_PAAMAYIM_NEKUDOTAYIM);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		$method = $phpcsFile->findNext(T_STRING, $stackPtr + 1);
		if ($tokens[$method]['content'] !== 'getInstance') {
			return;
		}

		$class = $phpcsFile->findPrevious(T_STRING, $stackPtr - 1);
		if (!preg_match('/(Get|Post|Cookie|Server)Registry$/i', $tokens[$class]['content'])) {
			// Note:  SessionRegistry is not provided by App
			return;
		}

		// Exceptions for certain files
		if ($this->isExceptedFile($phpcsFile->getFilename())) {
			return;
		}

		if ($phpcsFile->getCondition($stackPtr, T_CLASS) === FALSE &&
			$phpcsFile->getCondition($stackPtr, T_FUNCTION) === FALSE) {

			$phpcsFile->addWarning(
				'Singleton method %s::%s() should not be called here; instead, use App to inject the %s instance',
				$class,
				'FoundInGlobalScope',
				array(
					$tokens[$class]['content'],
					$tokens[$method]['content'],
					$tokens[$class]['content'],
				)
			);
		} else {
			$phpcsFile->addError(
				'Call to singleton method %s::%s() not allowed here; instead, use App to inject the %s instance',
				$class,
				'FoundInRestrictedScope',
				array(
					$tokens[$class]['content'],
					$tokens[$method]['content'],
					$tokens[$class]['content'],
				)
			);
		}
	}

	private function isExceptedFile($file) {
		if (preg_match('/lib\/bootstrap\.php$/', $file)) {
			return TRUE;
		}
		if (preg_match('/lib\/tests\/test_header\.php$/', $file)) {
			return TRUE;
		}
		if (preg_match('/lib\/cron\//', $file)) {
			return TRUE;
		}
		if (preg_match('/lib\/bin\//', $file)) {
			return TRUE;
		}

		return FALSE;
	}

	private function getPreferredUsage($superglobal) {
		static $preferred = array(
			'$_GET' => 'use GetRegistry::get() (use App to inject the GetRegistry, if needed)',
			'$_POST' => 'use PostRegistry::get() (use App to inject the PostRegistry, if needed)',
			'$_COOKIE' => 'use CookieRegistry::get() (use App to inject the CookieRegistry, if needed)',
			'$_SERVER' => 'use ServerRegistry::get() (use App to inject the ServerRegistry, if needed)',
			'$_REQUEST' => 'use the appropriate registry object, e.g. GetRegistry (use App to inject the registry, if needed)',
			'$_ENV' => 'see an Architect for the correct approach to solving this problem',
			'$_SESSION' => 'use SessionRegistry::get()',
		);

		return $preferred[$superglobal];
	}
}
