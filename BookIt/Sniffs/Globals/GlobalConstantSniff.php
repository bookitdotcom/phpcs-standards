<?php

namespace BookIt\Sniffs\Globals;

use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_Tokens;

class GlobalConstantSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_STRING, T_CONST);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		if ($tokens[$stackPtr]['code'] === T_STRING) {
			if ($tokens[$stackPtr]['content'] !== 'define') {
				return;
			}

			$phpcsFile->addWarning(
				'Constants should not be defined in the global scope unless absolutely necessary',
				$stackPtr,
				'Found'
			);

		} elseif ($tokens[$stackPtr]['code'] === T_CONST) {
			$namespace = $phpcsFile->getCondition($stackPtr, T_NAMESPACE);

			if (!isset($tokens[$namespace]['scope_opener'])) {
				return;
			}

			$nextPtr = $phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, $namespace + 1, $tokens[$namespace]['scope_opener'] + 1, TRUE);
			if ($nextPtr === $tokens[$namespace]['scope_opener']) {
				$phpcsFile->addWarning(
					'Constants should not be defined in the global scope unless absolutely necessary',
					$stackPtr,
					'Found'
				);
			}
		}
	}
}
