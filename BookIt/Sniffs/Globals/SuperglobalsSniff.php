<?php

namespace BookIt\Sniffs\Globals;

use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Sniff;

class SuperglobalsSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_VARIABLE);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		// Only process superglobals
		switch ($tokens[$stackPtr]['content']) {
			case '$_GET':
			case '$_POST':
			case '$_COOKIE':
			case '$_SERVER':
			case '$_REQUEST':
			case '$_ENV':
			case '$_SESSION':
				break;
			default:
				return;
		}

		// Exceptions for certain files
		if ($this->isExceptedFile($phpcsFile->getFilename())) {
			return;
		}

		if ($phpcsFile->getCondition($stackPtr, T_CLASS) === FALSE &&
			$phpcsFile->getCondition($stackPtr, T_FUNCTION) === FALSE) {

			$phpcsFile->addWarning(
				'Superglobal variable "%s" should not be used here; instead, %s',
				$stackPtr,
				'FoundInGlobalScope',
				array(
					$tokens[$stackPtr]['content'],
					$this->getPreferredUsage($tokens[$stackPtr]['content']),
				)
			);
		} else {
			$phpcsFile->addError(
				'Superglobal variable "%s" is not allowed in this scope; instead, %s',
				$stackPtr,

				'FoundInRestrictedScope',
				array(
					$tokens[$stackPtr]['content'],
					$this->getPreferredUsage($tokens[$stackPtr]['content']),
				)
			);
		}
	}

	private function isExceptedFile($file) {
		if (preg_match('/lib\/bootstrap\.php$/', $file)) {
			return TRUE;
		}
		if (preg_match('/lib\/tests\/test_header\.php$/', $file)) {
			return TRUE;
		}
		if (preg_match('/lib\/cron\//', $file)) {
			return TRUE;
		}
		if (preg_match('/lib\/bin\//', $file)) {
			return TRUE;
		}

		return FALSE;
	}

	private function getPreferredUsage($superglobal) {
		static $preferred = array(
			'$_GET' => 'use GetRegistry::get() (use App to inject the GetRegistry, if needed)',
			'$_POST' => 'use PostRegistry::get() (use App to inject the PostRegistry, if needed)',
			'$_COOKIE' => 'use CookieRegistry::get() (use App to inject the CookieRegistry, if needed)',
			'$_SERVER' => 'use ServerRegistry::get() (use App to inject the ServerRegistry, if needed)',
			'$_REQUEST' => 'use the appropriate registry object, e.g. GetRegistry (use App to inject the registry, if needed)',
			'$_ENV' => 'see an Architect for the correct approach to solving this problem',
			'$_SESSION' => 'use SessionRegistry::get()',
		);

		return $preferred[$superglobal];
	}
}
