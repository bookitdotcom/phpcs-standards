<?php

namespace BookIt\Sniffs\Classes;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class MethodOrderSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_CLASS);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		// By default, our next method can be any of these
		$allowed = array(
			'public' => TRUE,
			'protected' => TRUE,
			'private' => TRUE,
		);

		$lastMethod = NULL;
		$cPtr = $stackPtr;
		while (($cPtr = $phpcsFile->findNext(T_FUNCTION, $cPtr + 1, $tokens[$stackPtr]['scope_closer'])) !== FALSE) {
			$ctoken = $tokens[$cPtr];

			$method = $phpcsFile->getMethodProperties($cPtr);
			$scope  = $method['scope'];

			if (isset($allowed[$scope])) {
				if ($allowed[$scope] !== TRUE) {
					$error = 'Class method %s::%s() not allowed here; %s methods must be defined before %s methods';
					$data = array(
						$phpcsFile->getDeclarationName($stackPtr),
						$phpcsFile->getDeclarationName($cPtr),
						$scope,
						$this->getAllowedScopesString($allowed),
					);
					$phpcsFile->addError($error, $cPtr, 'ClassMethodsOutOfOrder', $data);
				} elseif ($lastMethod != $scope) {
					switch ($scope) {
						case 'private':
							$allowed['protected'] = FALSE;
							// No break
						case 'protected':
							$allowed['public'] = FALSE;
							break;
					}
				}

				// Skip the function body (avoids nested functions)
				if (isset($ctoken['scope_closer'])) {
					$cPtr = $ctoken['scope_closer'];
				}

				$lastMethod = $scope;
			}
		}
	}

	protected function getAllowedScopesString(array $allowed) {
		$out = array();
		foreach ($allowed as $scope => $isAllowed) {
			if ($isAllowed) {
				$out[] = $scope;
			}
		}

		return implode(', ', $out);
	}

}
