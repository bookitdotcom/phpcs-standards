<?php

namespace BookIt\Sniffs\Classes;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class ClassOrderSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_CLASS);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		// By default, our next token can be any of these
		$allowed = array(
			T_CONST => TRUE,    // Constants come first
			T_VARIABLE => TRUE, // ...then class variables
			T_FUNCTION => TRUE, // ...then class methods
		);

		$lastToken = NULL;
		$cPtr = $stackPtr;
		while (($cPtr = $phpcsFile->findNext(array(T_CONST, T_VARIABLE, T_FUNCTION), $cPtr + 1, $tokens[$stackPtr]['scope_closer'])) !== FALSE) {
			$ctoken = $tokens[$cPtr];
			if (isset($allowed[$ctoken['code']])) {
				if ($allowed[$ctoken['code']] !== TRUE) {
					// Exception: Variables in method declarations
					if (isset($ctoken['nested_parenthesis'])) {
						$parens = $ctoken['nested_parenthesis'];
						$paren = array_shift($parens);
						if ($tokens[$tokens[$paren]['parenthesis_owner']]['code'] === T_FUNCTION) {
							continue;
						}
					}

					$error = 'Class %s is not allowed here; must come before %s';
					$data = array(
						$this->translateTokenCode($ctoken['code']),
						$this->getAllowedTokensString($allowed),
					);
					$phpcsFile->addError($error, $cPtr, 'ClassOutOfOrder', $data);
					continue;

				} elseif ($lastToken != $ctoken['code']) {
					switch ($ctoken['code']) {
						case T_FUNCTION:
							$allowed[T_VARIABLE] = FALSE;
							// No break
						case T_VARIABLE:
							$allowed[T_CONST] = FALSE;
							break;
					}
				}

				// In a function, skip the function body
				if ($ctoken['code'] === T_FUNCTION && isset($ctoken['scope_closer'])) {
					$cPtr = $ctoken['scope_closer'];
				}

				$lastToken = $ctoken['code'];
			}
		}
	}

	protected function translateTokenCode($code, $plural = FALSE) {
		$plural = ($plural === TRUE);

		switch ($code) {
			case T_CONST:
				return $plural ? 'constants' : 'constant';
			case T_VARIABLE:
				return $plural ? 'variables' : 'variable';
			case T_FUNCTION:
				return $plural ? 'methods' : 'method';
		}

		return '';
	}

	protected function getAllowedTokensString(array $allowed) {
		$out = array();
		foreach ($allowed as $token => $isAllowed) {
			if ($isAllowed) {
				$out[] = $this->translateTokenCode($token, TRUE);
			}
		}

		return implode(', ', $out);
	}

}
