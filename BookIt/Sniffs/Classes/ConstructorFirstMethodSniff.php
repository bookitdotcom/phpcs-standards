<?php

namespace BookIt\Sniffs\Classes;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class ConstructorFirstMethodSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_CLASS);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();
		$className = $phpcsFile->getDeclarationName($stackPtr);

		$methodNumber = 0;
		for ($i = $stackPtr; $i < count($tokens); $i++) {
			$ctoken = $tokens[$i];
			if ($ctoken['code'] === T_FUNCTION) {
				$methodName = $phpcsFile->getDeclarationName($i);
				if (($methodName == '__construct' || $methodName == $className) && $methodNumber > 0) {
					$error = 'Class constructor %s::%s() must be the first method defined in the class';
					$data = array(
						$className,
						$methodName,
					);
					$phpcsFile->addError($error, $i, 'ConstructorNotFirst', $data);
				}
				$methodNumber++;
			}
		}
	}

}
