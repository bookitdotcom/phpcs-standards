<?php

namespace BookIt\Sniffs\Classes;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class PropertyOrderSniff implements PHP_CodeSniffer_Sniff {

	public function register() {
		return array(T_CLASS);
	}

	public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();

		// By default, our next method can be any of these
		$allowed = array(
			'public' => TRUE,
			'protected' => TRUE,
			'private' => TRUE,
		);

		$lastMethod = NULL;
		$cPtr = $stackPtr;
		while (($cPtr = $phpcsFile->findNext(T_VARIABLE, $cPtr + 1, $tokens[$stackPtr]['scope_closer'])) !== FALSE) {
			$ctoken = $tokens[$cPtr];

			// Make sure it's a class variable
			$conditions = array_keys($ctoken['conditions']);
			$condition = array_pop($conditions);
			if (!isset($tokens[$condition]) || $tokens[$condition]['code'] !== T_CLASS || isset($ctoken['nested_parenthesis'])) {
				return;
			}

			$props = $phpcsFile->getMemberProperties($cPtr);
			$scope  = $props['scope'];

			if (isset($allowed[$scope])) {
				if ($allowed[$scope] !== TRUE) {
					$error = 'Class property %s::%s() not allowed here; %s properties must be defined before %s properties';
					$data = array(
						$phpcsFile->getDeclarationName($stackPtr),
						$tokens[$cPtr]['content'],
						$scope,
						$this->getAllowedScopesString($allowed),
					);
					$phpcsFile->addError($error, $cPtr, 'ClassPropertiesOutOfOrder', $data);
				} elseif ($lastMethod != $scope) {
					switch ($scope) {
						case 'private':
							$allowed['protected'] = FALSE;
							// No break
						case 'protected':
							$allowed['public'] = FALSE;
							break;
					}
				}

				$lastMethod = $scope;
			}
		}
	}

	protected function getAllowedScopesString(array $allowed) {
		$out = array();
		foreach ($allowed as $scope => $isAllowed) {
			if ($isAllowed) {
				$out[] = $scope;
			}
		}

		return implode(', ', $out);
	}

}
